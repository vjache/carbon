lint:
	hlint .

compile:
	rm -rf dist/
	cabal configure
	cabal build

test: compile
	(cd src && runhaskell TestMemWM.hs)

run: compile
	dist/build/Carbon/Carbon one two three
