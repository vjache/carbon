-- file TestMemWM.hs
import Test.Hspec
import Test.QuickCheck

import Control.Exception (evaluate)
import Control.Applicative((<$>))
import Control.Monad.State.Lazy
import qualified Data.Set as Set

import Language.Carbon.Model
import Language.Carbon.WorkingMemory
import Language.Carbon.MemWM


main :: IO ()
main = hspec $ do
         describe "Language.Carbon.MemWM" $ do
                 it "update . Assert" $ do
                   testUpdateAssert `shouldSatisfy` (3 ==) . length
                 it "assertMany" $ do
                   testAssertMany `shouldSatisfy` (3 ==) . length
                 it "matchSentence >>= retrieveSolutions" $ do
                   testMatchSentenceAndRetrieve `shouldSatisfy` (1 ==) . length
                 it "joinSolutionSets" $ do
                   testJoinSolutionSets `shouldSatisfy` (1 ==) . length

wi :: Int -> Word
wi = toWord
-------------------------------------------------------------------------------
-- TEST CASEs 
-------------------------------------------------------------------------------

-- AssertMany
testAssertMany = evalMemWM $ 
       do
         assertMany [ [Key "k1", wi 1, wi 1, wi 1],
                      [Key "k2", wi 2, wi 2, wi 2],
                      [Key "k3", wi 3, wi 3, wi 3]]
         Set.toList <$> get
-- Update Assert
testUpdateAssert = evalMemWM $ 
       do
         update $ Assert [Key "k1", wi 1, wi 1, wi 1]
         update $ Assert [Key "k2", wi 2, wi 2, wi 2]
         update $ Assert [Key "k3", wi 3, wi 3, wi 3]
         Set.toList <$> get
-- Match Sentence & Retrieve
testMatchSentenceAndRetrieve = evalMemWM $ 
       do
         assertMany [[Key "k1", wi 1, wi 2, wi 3],
                     [Key "k1", wi 2, wi 2, wi 3]]
         matchSentence [Key "k1", C $ I 1, Var INT "v1", Var INT "v2"] 
                           >>= retrieveSolutions

-- Join Solution Sets & Retrieve
testJoinSolutionSets = evalMemWM $ 
       do
         assertMany 
                   [ 
                    [Key "k1", wi 1, wi 2, wi 3],
                    [Key "k1", wi 2, wi 2, wi 4],
                    [Key "k2", wi 1, wi 2, wi 3],
                    [Key "k2", wi 2, wi 2, wi 5]
                   ]
         sols1 <- matchSentence [Key "k1", Var INT "x", wi 2, Var INT "v"] 
         sols2 <- matchSentence [Key "k2", Var INT "y", wi 2, Var INT "v"] 
         joinSolutionSets sols1 sols2 >>= retrieveSolutions
