{-# LANGUAGE PostfixOperators,
             FlexibleInstances #-}
module Language.Carbon.DSL
    (
     action,
     goal,
     if_,
     iff,
     when_,
     guard_,
     effect,
     evalRuleBuilder,
     k,
     (?),
     (?!),
     (?&),
     (?@),
     (!),
     (&),
     (#)
    ) where

import Language.Carbon.Model
import Language.Carbon.PP
import Control.Applicative((<$>))
import Control.Monad.Trans.State

import Debug.Trace
-----------------------------------------------------------------------------------------
-- Data & Classes & Instances
-----------------------------------------------------------------------------------------
data DestSwitch = DestCondition | DestEffect | DestNo  deriving Show
data RuleBuilder = RuleBuilder {rbDest :: DestSwitch, 
                                rbArg :: Rule [Word]} deriving Show
type RuleBuilderState = State RuleBuilder
--
class Monad ml => ModifyLog ml where
    logMdf :: Sentence s => Modify s -> ml ()
--
instance ModifyLog (State RuleBuilder) where
    logMdf ms = do
      r <- gets rbArg
      d <- gets rbDest
      case d of
        DestEffect ->
            modify $ \ab@RuleBuilder{rbArg=act@Action{actionEffect=eff}} ->
                ab{rbArg = act{actionEffect=eff ++ [toWords <$> ms]}}
        DestCondition ->
            modify $ \ab@RuleBuilder{
                         rbArg = r} ->
                   case r of
                     Action{ruleCondition = cond,
                            actionEffect  = eff} ->
                          if isAssert ms 
                          then ab{rbArg = r{ruleCondition = cond++[toWords $ mdfArg ms]}}
                          else ab{rbArg = r{ruleCondition = cond++[toWords $ mdfArg ms],
                                            actionEffect  = eff++[toWords <$> ms]}}
                     _ ->
                         ab{rbArg = r{ruleCondition = 
                                          (ruleCondition r)++[toWords $ mdfArg ms]}}
--
instance WordMorph Word where
    toWord = id
--
instance WordMorph [Word] where
    toWord ws = 
        case ws of
          [Key "-", x, y] -> Sub x y
          [Key "/", x, y] -> Div x y
          Key "+":xs      -> Sum xs
          Key "*":xs      -> Mul xs
          [Key "-", x]    -> Neg x
          otherwise       -> error "Can not morph sentence to word"
-----------------------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------------------
evalRuleBuilder :: RuleBuilderState () -> Rule [Word]
evalRuleBuilder rbs =
    rbArg $ execState rbs $ RuleBuilder{ rbDest = DestNo,
                                         rbArg  = undefined}
--
action :: Sentence s => 
          s -> RuleBuilderState ()
action s = do
  modify $ \rb -> 
      rb{rbArg  = Action{ruleSignature = toWords s,
                         ruleCondition = [],
                         ruleGuard     = And [],
                         actionEffect  = []}}
--
goal :: Sentence s => 
          s -> RuleBuilderState ()
goal s = do
  modify $ \rb -> 
      rb{rbArg  = Goal{ruleSignature = toWords s,
                         ruleCondition = [],
                         ruleGuard     = And []}}
--
iff :: Sentence s => 
          s -> RuleBuilderState ()
iff s = do
  modify $ \rb -> 
      rb{rbArg  = Iff{ruleSignature = toWords s,
                      ruleCondition = [],
                      ruleGuard     = And []}}
if_ :: Sentence s => 
          s -> RuleBuilderState ()
if_ s = do
  modify $ \rb -> 
      rb{rbArg  = If{ruleSignature = toWords s,
                     ruleCondition = [],
                     ruleGuard     = And []}}
--
when_ :: RuleBuilderState ()
when_ = do
    trace "When_" $ modify $ \ab ->
        ab{rbDest=DestCondition}
--    gets ((Assert <$>) . ruleCondition . rbArg)
--
effect :: RuleBuilderState ()
effect = do
    trace "Effect" $ modify $ \ab ->
        ab{rbDest=DestEffect}
--    gets (actionEffect . rbArg)
--
guard_ :: Guard [Word] -> RuleBuilderState ()
guard_ g =
  trace "Guard" $ modify $ \ab@RuleBuilder{rbArg=act} ->
      ab{rbArg=act{ruleGuard=g}}
--
iword :: Int -> Word
iword = toWord

sword :: String -> Word
sword = toWord

svar :: String -> Word
svar = Var STR

ivar :: String -> Word
ivar = Var INT
--
--
(?) :: [Word] -> String -> [Word]
(?) s nm = s ++ [Var STR nm]
--
(?!) :: [Word] -> String -> [Word]
(?!) s nm = s ++ [Var INT nm]
--
(?&) :: [Word] -> String -> [Word]
(?&) s nm = s ++ [Var BOOL nm]
--
(?@) :: [Word] -> String -> [Word]
(?@) s nm = s ++ [Var FLOAT nm]
--
(!) :: WordMorph w => [Word] -> w -> [Word]
(!) s w = s ++ [toWord w]
--1
k :: String -> [Word]
k nm = [Key nm]
--
(&) :: (ModifyLog ml, Sentence s) => ml () -> s -> ml ()
(&) mxs x = do 
  () <- mxs
  logMdf $ Assert $ toWords x

(#) :: (ModifyLog ml, Sentence s) => ml () -> s -> ml ()
(#) mxs x = do 
  () <- mxs
  logMdf $ Retire $ toWords x
-----------------------------------------------------------------------------------------
-- Examples
-----------------------------------------------------------------------------------------
example1 = putPpLn $ ppRule $ evalRuleBuilder $ do
         action (k"move" ?"ratId" ?"cellId")
         when_ # (k"Rat"  ?"ratId" ?!"ratE")
              # (k"At"   ?"ratId" ?"cellId0")
              # (k"Free" ?"cellId")
              & (k"Cell" ?"cellId")
              & (k"Cell" ?"cellId0")
              & (k"Near" ?"cellId" ?"cellId0")
         guard_ $ Pred (k"gr" ?!"ratE" !I 0 )
         effect & (k"Rat"  ?"ratId" !(k"-" ?!"ratE" !I 1) )
                & (k"Free" ?"cellId0")
                & (k"At"   ?"ratId" ?"cellId")
--
example2 = putPpLn $ ppRule $ evalRuleBuilder $ do
  if_   (k"Near" ?"cid0" ?"cid1")
  when_ &(k"Near" ?"cid1" ?"cid0")
