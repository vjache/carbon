{- | 
   This module handles several tasks:
   * Define data types to represent working memory data in a unified way 
     as a collection of sentences. Each sentence is a list of words and 
     words are a simpliest bits of data.
   * Define a type classes bridging the user data types defined 
     in a classical, haskelish, well-typed way with a "world of words". 
     Of course user code may use words as its native data representation, 
     but it may be not the best idea due to a words are less type safe. So 
     the recomended way for user code to operate with strongly typed domain 
     specific data types and implement a bunch of instances to be able to 
     morph to words (i.e. build morphism from DSL to "world of words".
   * Define data types to represent a rules of transition and a module as a 
     collection of rules, goals and helper definitions. 
-}
{-# LANGUAGE MultiParamTypeClasses, 
             FunctionalDependencies, 
             DeriveFunctor,
             StandaloneDeriving,
             FlexibleInstances,
             DeriveGeneric #-}
module Language.Carbon.Model
    (Domain(..),
     Word(..),
     VarBindings(..),
     WordMorph(..),
     Sentence(..),
     Solution(..),
     Substitutable(..),
     ArithEvaluable(..),
     Modify(..),
     Module(..),
     Rule(..),
     Guard(..),
     domainOf,
     isVar,
     asserts,
     retires,
     isAssert,
     isRetire,
     mdfPartition,
     mdfOp,
     isAction,
     isGoal,
     isIff,
     isIf,
     newModule,
     evalGuard,
     matchPattern) where

{- IMPORT SECTION-}
import Control.Monad
import Data.List
import GHC.Generics (Generic)
import Data.Hashable
import Data.Maybe
import Control.Applicative((<*), (<$>), (<*>))
import Prelude hiding (negate, subtract)
import Control.DeepSeq
{----------------------------- 
   Fundamental Types & Classes 
 ------------------------------}
-- Data type for data types reification  
data Domain   = KEYWORD | STR | INT | FLOAT | BOOL | TUPLE |
                Domain String deriving (Eq, Show, Ord, Generic)
instance NFData Domain
-- Generic word structure
data Word =
    Key String        |
    Var Domain String |
-- Consts
    I Int         | -- integer
    F Float       | -- float
    B Bool        | -- boolean
    S String      | -- string
    T [Word]      | -- tuple
    Nil Domain    | -- nil, null, nothing
    E Int Domain  | -- enumeration member
-- Arith's
    Neg Word      | 
    Sub Word Word |
    Sum [Word]    |
    Mul [Word]    |
    Div Word Word |
    Pow Word Word |
    Max [Word]    |
    Min [Word]    |
    Avg [Word]    |
    Sqr  Word     |
    Sqrt Word     |
    Sin  Word     |
    Cos  Word     |
    Exp  Word
    deriving (Eq, Show, Ord, Generic)
instance NFData Word
--
wmap f0 w = 
    case w of
    Neg w   -> Neg  $ f w
    Sub w w1 -> Sub  (f w) (f w1)
    Sum ws  -> Sum  $ f <$> ws
    Mul ws  -> Mul  $ f <$> ws
    Div w w1 -> Div  (f w) (f w1)
    Pow w w1 -> Pow  (f w) (f w1)
    Max ws  -> Max  $ f <$> ws
    Min ws  -> Min  $ f <$> ws
    Avg ws  -> Avg  $ f <$> ws
    Sqr  w  -> Sqr  $ f w
    Sqrt w  -> Sqrt $ f w
    Sin  w  -> Sin  $ f w
    Cos  w  -> Cos  $ f w
    Exp  w  -> Exp  $ f w
    w       -> f0 w
    where f = wmap f0
--
negate (I i) = I (-i)
--
subtract (F a) (F b) = F $ a - b
subtract (I a) (I b) = I $ a - b
subtract (I a) (F b) = F $ (fromIntegral a) - b
subtract (F a) (I b) = F $ a - (fromIntegral b)
subtract a b = error $ "Can not subtract: " ++ (show a) ++ ", " ++ (show b)
--
divide (F a) (F b) = F $ a / b
divide (I a) (I b) = I $ a `div` b
divide (I a) (F b) = F $ (fromIntegral a) / b
divide (F a) (I b) = F $ a / (fromIntegral b)
--
summ (F a) (F b) = F $ a + b
summ (I a) (I b) = I $ a + b
summ (F a) (I b) = F $ a + (fromIntegral b)
summ (I a) (F b) = F $ (fromIntegral a) + b
--
mult (F a) (F b) = F $ a * b
mult (I a) (I b) = I $ a * b
mult (F a) (I b) = F $ a * (fromIntegral b)
mult (I a) (F b) = F $ (fromIntegral a) * b
--
power (F a) (I b) = F $ a ^ b
power (I a) (I b) = F $ (fromIntegral a) ^ b
power (F a) (F b) = F $ exp $ b * log a
power (I a) (F b) = F $ exp $ b * log (fromIntegral a)
power a b = error $ "Can not power: " ++ (show a) ++ ", " ++ (show b)
--
--
--
class ArithEvaluable e where
    evalArith :: e -> e

instance (Functor f, ArithEvaluable e) => ArithEvaluable (f e) where
    evalArith f = evalArith <$> f

instance ArithEvaluable Word where
    evalArith (Neg ar)    = negate $ evalArith ar
    evalArith (Sub a b)   = subtract (evalArith a) (evalArith b)
    evalArith (Div a b)   = divide (evalArith a) (evalArith b)
    evalArith (Sum (h:t)) = foldr summ (evalArith h) (evalArith <$> t)
    evalArith (Mul (h:t)) = foldr mult (evalArith h) (evalArith <$> t)
    evalArith (Pow a b)   = power (evalArith a) (evalArith b)
    evalArith (Max (h:t)) = 
        let mx a b = max a b
        in foldr mx (evalArith h) (evalArith <$> t)
    evalArith (Min (h:t)) = 
        let mn a b = min a b
        in foldr mn (evalArith h) (evalArith <$> t)
    evalArith (Avg l) = evalArith $ Div (Sum l) (I $ length l)
    evalArith w           = w

--instance Substitutable (Arith Word)
--
test0 =
    let vb   = [(Var INT "x", I 1), (Var FLOAT "y", F 1)] :: [(Word, Word)]
    in substitute vb $ Sum [Var INT "x", Sub (Var FLOAT "y") (I 3)]
evalArithTest =
    let sent = [Key "k1", 
                Sum [Var INT "x", 
                     Sub (Sum [(Var FLOAT "y"), 
                               (Pow (Var INT "x") (I 2))]) (I 3)]] :: [Word]
        vb   = [(Var INT   "x", I 2), 
                (Var FLOAT "y", F 1.5)] :: [(Word, Word)]
    in evalArith (substitute vb sent)
--
isVar :: Word -> Bool
isVar (Var _ _) = True
isVar _         = False
--
isKey :: Word -> Bool
isKey (Key _) = True
isKey _       = False
--
isConst :: Word -> Bool
isConst w = 
    case w of
      Nil _ -> True
      I _   -> True
      F _   -> True
      B _   -> True
      S _   -> True
      T _   -> True
      E{}   -> True
      _     -> False
-- Concrete word
type VarBindings = [(Word,Word)]
-- Extract domain from data types
class HaveDomain d where
    domainOf :: d -> Domain
instance HaveDomain Word where
    domainOf (Key _)   = KEYWORD
    domainOf (Var d _) = d
    domainOf w     = 
        case w of
          Nil d -> d
          I _ -> INT
          F _ -> FLOAT
          B _ -> BOOL
          S _ -> STR
          T _ -> TUPLE   
-- Convert data types to concrete words i.e. :: GenWord Const
class WordMorph w where
    toWord :: w -> Word
instance WordMorph Int where
    toWord = I
instance WordMorph Float where
    toWord = F
instance WordMorph Bool where
    toWord = B
instance WordMorph String where
    toWord = S
-- Substitute solution to the entity
class Substitutable s where 
    substitute :: Solution sl => sl -> s -> s
-- List of words is a substitutable entity
instance Substitutable Word where
    substitute sl = wmap (\w -> maybeLookupValue sl w w)
--
instance Substitutable s => Substitutable (Rule s) where
    substitute sl = (substitute sl <$>)
--
instance Substitutable s => Substitutable (Guard s) where
    substitute sl = (substitute sl <$>)
--
instance Substitutable s => Substitutable (Modify s) where
    substitute sl = (substitute sl <$>)
--
instance Substitutable s => Substitutable [s] where
    substitute sl = (substitute sl <$>)
-- Convert user data types to concrete words i.e. :: [GenWord Const]
class Sentence s where
    toWords    :: s -> [Word]
    -- helpers
    vars       :: s -> [Word]
    keys       :: s -> [Word]
    consts     :: s -> [Word]
    arity      :: s -> Int
    varsArity  :: s -> Int
    constArity :: s -> Int
    -- dafault impls
    arity      = length . toWords
    vars       = (filter isVar) . toWords
    keys       = (filter isKey) . toWords
    consts     = (filter isConst) . toWords
    varsArity  = length . vars
    constArity = length . consts
--
instance Sentence [Word] where
    toWords w = w
--
class Solution sl where
    lookupValue  :: sl -> Word -> Maybe Word
    getBindings  :: sl -> [(Word,Word)]
    maybeLookupValue :: sl -> Word -> Word -> Word
    maybeLookupValue sl wordKey wordFallback = 
        maybe wordFallback id $ lookupValue sl wordKey
    getValue     :: sl -> Word -> Word
    getValue sl = fromJust . (lookupValue sl)
--
instance Solution [(Word, Word)] where
    lookupValue = flip lookup
    getBindings = id
--
----------------------------------------------------------------------
instance Hashable Domain

instance Hashable Word
----------------------------------------------------------------------
data Modify s = Assert {mdfArg :: s} | 
                Retire {mdfArg :: s}  deriving (Eq, Ord, Show, Functor, Generic)
instance NFData s => NFData (Modify s)
--
isAssert :: Modify s -> Bool
isAssert (Assert _) = True
isAssert _          = False
--
isRetire :: Modify s -> Bool
isRetire (Assert _) = True
isRetire _          = False
--
retires :: [Modify s] -> [Modify s]
retires [] = []
retires (m:ml) = case m of
                   Assert _ -> retires ml
                   Retire _ -> m:retires ml
--
asserts :: [Modify s] -> [Modify s]
asserts [] = []
asserts (m:ml) = case m of
                   Assert _ -> m:asserts ml
                   Retire _ -> asserts ml
--
mdfOp :: Modify t -> s -> Modify s
mdfOp (Assert _) = Assert
mdfOp (Retire _) = Retire
--
mdfPartition :: [Modify s] -> ([Modify s], [Modify s])
mdfPartition = partition isAssert
--
data Rule s = 
    Action 
    { 
      ruleSignature :: s,
      ruleCondition :: [s],
      ruleGuard     :: Guard s,
      actionEffect  :: [Modify s]
    } | 
    Goal {
      ruleSignature :: s,
      ruleCondition :: [s],
      ruleGuard     :: Guard s
    } | 
    Iff {
      ruleSignature :: s,
      ruleCondition :: [s],
      ruleGuard     :: Guard s
    } | 
    If {
      ruleSignature :: s,
      ruleCondition :: [s],
      ruleGuard     :: Guard s
    }
    deriving (Eq, Ord, Show, Functor, Generic)
instance NFData s => NFData (Rule s)
{-
  Iff in_order(X) 
  then
     wheel_of(X, Y),
     wheel_of(X, Z),
     wheel_of(X, W),
     wheel_of(X, Q), 
     in_order(Y),
     in_order(Z),
     in_order(W),
     in_order(Q),
  
 -}
--
isAction Action{} = True
isAction _        = False
--
isGoal Goal{} = True
isGoal _      = False
--
isIff Iff{} = True
isIff _     = False
--
isIf If{} = True
isIf _     = False
--
data Module s = Module
    {
      modDomains   :: [(String, Domain)],
      modSentences :: [s],
      modRules     :: [Rule s]
    } deriving (Eq, Show, Functor)
--
newModule :: Module s
newModule = Module
    {
      modDomains   = [],
      modSentences = [],
      modRules     = []
    }
--------------------------------------------------------------
-- Guard section
--------------------------------------------------------------
data Guard s = And [Guard s] | 
               Or  [Guard s]  | 
               Pred s 
               deriving (Ord, Eq, Show, Functor, Generic)
instance NFData s => NFData (Guard s)
--
evalWith :: (Substitutable s, 
             Sentence s) => VarBindings -> Guard s -> Bool
evalWith vb g = 
    evalGuard $ substitute vb g
--
evalGuard :: (Sentence s) => Guard s -> Bool
evalGuard (And gs) = and $ evalGuard <$> gs
evalGuard (Or  gs) = or  $ evalGuard <$> gs
evalGuard (Pred s) = 
    case toWords s of
      [Key "eq",v1,v2] -> 
          v1 == v2
      Key "eq":w1:ws -> 
          if null $ dropWhile (w1==) ws
          then True
          else False
      [Key "neq",v1,v2] -> 
          v1 /= v2
      Key "neq":w1:ws -> 
          if  all (w1/=) ws
          then True
          else False
      Key "distinct":ws -> 
          if (length ws) == (length $ nub ws)
          then True
          else False
      [Key "gr",v1,v2] -> 
          v1 > v2
      Key "gr":w1:ws -> 
          if all (w1>) ws
          then True
          else False
      [Key "ls",v1,v2] -> 
          v1 < v2
      Key "ls":w1:ws -> 
          if all (w1<) ws
          then True
          else False
      [Key "gre",v1,v2] -> 
          v1 >= v2
      Key "gre":w1:ws -> 
          if all (w1>=) ws
          then True
          else False
      [Key "lse",v1,v2] -> 
          v1 <= v2
      Key "lse":w1:ws -> 
          if all (w1<=) ws
          then True
          else False
      Key "in":v:l:r:[] -> 
          l <= v && v <= r
      Key "inl":v:l:r:[] -> 
          l < v && v <= r
      Key "inr":v:l:r:[] -> 
          l <= v && v < r
      Key "inlr":v:l:r:[] -> 
          l < v && v < r
      Key "oneOf":v:ws -> 
          if elem v ws
          then True
          else False
      _ ->
          error $ "Unexpected logic predicate sentence: " ++ (show $ toWords s)
--
testEvalGuard vb =
    evalWith vb $ And [Pred [Key "eq", Var INT "x", I 0]]
--------------------------------------------------------------
-- Model utility functions.
--------------------------------------------------------------

-- | Match pattern against sentence and return a var bindings.
--   
-- 1. The first argument is considered to be a pattern i.e. sentence 
--    with vars 
-- 2. while the second one is consedered to be a source of concrete 
--    sentences i.e. with non var words.
-- 
-- The source of sentences (second arg) is a monadic value, so it is
-- possible to apply this function for at least two practical cases:
-- 1. source is a 'Maybe' type: matchPattern :: s -> Maybe s -> Maybe VarBindings
-- 2. source is a '[]'    type: matchPattern :: s -> [s] -> [VarBindings]
matchPattern :: (MonadPlus m, Sentence s) => s -> m s -> m VarBindings
matchPattern p ms = do 
  s <- ms
  res <- match (toWords p) (toWords s)
  guard $ check res
  return res
    where match [] [] = return []
          match [] _  = mzero
          match _ []  = mzero
          match (pw:pt) (sw:st) = do
            rest <- match pt st
            let bind = (pw, sw)
            case pw of
              (Var d _) | domainOf sw == d -> return $ bind:rest
              _         | pw == sw         -> return rest
                        | otherwise        -> mzero
          check [] = True
          check ((var, val):bs) =
            maybe (check bs) (\val1 -> 
                                  if   val /= val1 
                                  then False
                                  else check bs) $ lookup var bs

{-------------------------------------------------------------
  Example Types
 -------------------------------------------------------------}
