{-# LANGUAGE MultiParamTypeClasses, 
             FunctionalDependencies,
             FlexibleInstances, 
             DeriveFunctor #-}

module Language.Carbon.WorkingMemory.SolutionPlan
    (
     SolutionPlan(..),
     spJoin,
     spTerm,
     spRef,
     spIsTerm,
     spIsJoin,
     spIsRef,
     spChildren,
     spFromList,
     spApplySentence,
     spPP
    ) where

import Language.Carbon.Model
import qualified Data.MapOnSets as O
import Control.Monad
import qualified Data.Set as S
import qualified Data.Map as M
import Data.Maybe
import Data.List
import Control.Monad.Identity
import Control.Monad.State.Lazy
import Control.Applicative((<*), (<$>), (<*>))
import Data.Hashable
import Data.Function
--------------------------------------------------------------------
-- A solution set model
data SolutionPlan s =
    -- Matches concrete sentences against sentence pattern hence 
    -- forming a primary set of solutions for variables in a pattern.
    SpTerm{
      spId       :: Int,
      spVars     :: [Word],
      spSentence :: s} | 
    -- Joins a two other solution sets into a single one based on a 
    -- common variable between them.
    SpJoin{
      spId       :: Int,
      spVars     :: [Word],
      spJoinVars :: [Word],
      spLeft     :: SolutionPlan s, 
      spRight    :: SolutionPlan s } |
    SpRef{
      spId       :: Int} deriving (Show, Eq, Ord, Functor)
-- Join two solution sets
spJoin :: Sentence s => SolutionPlan s -> SolutionPlan s -> SolutionPlan s
spJoin sp1 sp2 = 
    let leftVars   = spVars sp1
        rightVars  = spVars sp2
        allVars    = nub $ leftVars ++ rightVars
        sharedVars = leftVars `intersect` rightVars
    in SpJoin{ spId       = spId sp1 `hashWithSalt` spId sp2,
                spLeft     = sp1,
                spRight    = sp2,
                spVars     = allVars,
                spJoinVars = sharedVars}
-- New term solution set based on sentence
spTerm :: Sentence s => s -> SolutionPlan s
spTerm s = SpTerm{
              spId       = hash $ toWords s,
              spVars     = filter isVar $ toWords s,
              spSentence = s}
--
spRef = SpRef
--
spIsTerm SpTerm{} = True
spIsTerm _ = False
--
spIsJoin SpJoin{} = True
spIsJoin _ = False
--
spIsRef SpRef{} = True
spIsRef _ = False
--
spCommonVars  :: Sentence s => SolutionPlan s -> SolutionPlan s -> [Word]
spCommonVars sp1 sp2 = 
    spVars sp1 `intersect` spVars sp2
--
-- Make a solution plan from a list of sencence patterns using startegy of 
-- "firstly join plans with maximal intersection of variables".
spFromList :: (Ord s, Sentence s) => [s] -> SolutionPlan s
spFromList ps | ps /= [] = 
               let choosePair spl = 
                       maximumBy (compare `on` fst) $ 
                                 [ (length $ spCommonVars sp1 sp2, [sp1, sp2]) | 
                                   sp1 <- spl, 
                                   sp2 <- spl, 
                                   sp1 > sp2]
                   reduce [sp] = sp
                   reduce spl  =
                       case choosePair spl of
                         (cvars, pair@[s1,s2]) 
                             | cvars > 0 ->
                                 reduce $ spJoin s1 s2:(spl \\ pair)
                             | otherwise ->
                                 foldl spJoin (head spl) (tail spl)
               in reduce $ spTerm <$> sort ps
--
spChildren :: Sentence s => SolutionPlan s -> [SolutionPlan s]
spChildren SpTerm{} = []
spChildren SpJoin{spLeft = l, spRight = r} = [l,r]
--
type SpCacheState = State (O.MapOnSets (Int,[Word]) VarBindings)
--
spApplySentence :: Sentence s => s -> SolutionPlan s -> SpCacheState [ VarBindings ]
spApplySentence s SpTerm{spId = mid, spSentence = p} =
  return $ matchPattern p [s]
spApplySentence s SpJoin{spJoinVars = jvars,
                           spVars     = vars,
                           spLeft     = l, 
                           spRight    = r} = do
  let cacheL = spId l
      cacheR = spId r
  vbsLeftIndexed  <- spApplySentence s l >>= indexate cacheL
  vbsRightIndexed <- spApplySentence s r >>= indexate cacheR
  ss1             <- join     vbsLeftIndexed cacheR lprod
  ss2             <- join     vbsRightIndexed cacheL (flip lprod)
  return $ format vars <$> ss1 ++ ss2
      where 
        key :: VarBindings -> [Word]
        key vb =  [lookupJust v vb | v <- jvars]
        -- |
        indexate :: Int -> [ VarBindings ] -> 
                    SpCacheState [([Word], VarBindings)]
        indexate _ [] = return []
        indexate cacheId (vb:vbs) = do
          let k = key vb
          modify $ O.insert (cacheId, k) vb
          ((k,vb):) <$> indexate cacheId vbs
        -- |
        cachedVbs k cacheId = gets $ O.lookup (cacheId,k)
        -- |
        lprod lset rset = [ x ++ y | 
                            x <- S.toList lset, 
                            y <- S.toList rset ]
        -- |
        join vbsIndexed cacheId  prod = 
            concat <$> (sequence $ [do vbs <- cachedVbs k cacheId
                                       return $ prod (S.singleton vb) vbs
                                    | (k,vb) <-  vbsIndexed])

lookupJust k l = fromJust $ lookup k l

format vars ss = do 
  v <- vars
  return (v, lookupJust v ss)
----------------------------------------------------------------------------
--
spPP :: (Show s) => String -> SolutionPlan s -> IO ()
spPP shift SpJoin{spLeft = l, spRight = r} = do
  putStrLn $ shift ++ "*:"
  spPP (shift ++ "  ") l
  spPP (shift ++ "  ") r
spPP shift SpTerm{spSentence = s} = do
  putStrLn $ shift ++ (show s)
------------------------------------------------------------------------------

evalTest t = evalState t M.empty

runTest t = runState t M.empty

execTest t = execState t M.empty

test1 = do
  let sp1 = spTerm [Var STR "name", Key "nameOf", Var INT "id"]
  spApplySentence [S "John", Key "nameOf", I 0] sp1

test2 = do
  let sp1 = spTerm [Var STR "name", Key "nameOf", Var INT "id"]
      sp2 = spTerm [Var STR "family", Key "familyOf", Var INT "id"]
      sp  = spJoin sp1 sp2
  spApplySentence [S "John", Key "nameOf", I 0] sp
  spApplySentence [S "Ramboo", Key "familyOf", I 0] sp

test3 = do
  let sp1 = spTerm [Var STR "name", Key "nameOf", Var INT "id"]
      sp2 = spTerm [Var STR "family", Key "familyOf", Var INT "id"]
      sp  = spJoin sp1 sp2
  spApplySentence [S "Ramboo", Key "familyOf", I 0] sp
  spApplySentence [S "John", Key "nameOf", I 0] sp

test4 = do
  let sp1 = spTerm [Var STR "name", Key "nameOf", Var INT "id"]
      sp2 = spTerm [Var STR "family", Key "familyOf", Var INT "id"]
      sp  = spJoin sp1 sp2
  sequence $ 
        [spApplySentence [S "Ramboo", Key "familyOf", I 0] sp,
         spApplySentence [S "John", Key "nameOf", I 0] sp,
         spApplySentence [S "Garry", Key "nameOf", I 1] sp,
         spApplySentence [S "Smith", Key "familyOf", I 1] sp]

test5 = do
  let sp1 = spTerm [Var STR "name", Key "nameOf", Var INT "id"]
      sp2 = spTerm [Var STR "name1", Key "nameOf", Var INT "id1"]
      sp3 = spTerm [Var INT "id", Key "friendOf", Var INT "id1"]
      sp  = sp1 `spJoin` sp2 `spJoin` sp3
  spApplySentence [S "Ramboo", Key "familyOf", I 0] sp
  spApplySentence [S "John", Key "nameOf", I 0] sp
  spApplySentence [S "Garry", Key "nameOf", I 1] sp
  spApplySentence [S "Smith", Key "familyOf", I 1] sp
  spApplySentence [I 0, Key "friendOf", I 1] sp
----------------------------------------------------------------------------
test6 = 
  let s1 = [Var STR "name", Key "nameOf", Var INT "id"] :: [Word]
      s2 = [Var STR "name1", Key "nameOf", Var INT "id1"]
      s3 = [Var INT "id", Key "friendOf", Var INT "id1"]
  in spFromList [s1, s2, s3]
----------------------------------------------------------------------------

--spNetApply :: Sentence s => s -> SpMapState s ()
