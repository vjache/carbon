{-# LANGUAGE MultiParamTypeClasses, 
             FunctionalDependencies,
             FlexibleInstances, 
             DeriveFunctor #-}
{-
  A Working Memory Automaton.
 -}
module Language.Carbon.WorkingMemory.Automaton
    (
     Automaton(..),
     AutomatonState(..),
     newAutomaton,
     compileModule,
     apply,
     interpret,
     handle,
     member,
     enlist,
     count,
     agenda,
     agendaHas,
     agendaLookup,
     goals
    ) where
-- Carbon modules
import Language.Carbon.Model
import Language.Carbon.HSentence
import Language.Carbon.WorkingMemory.SolutionPlan
import qualified Data.MapOnSets as O
import qualified Data.IntMapOnSets as I
import Language.Carbon.WorkingMemory.SpNet
import Language.Carbon.WorkingMemory.VarUnify
import qualified Language.Carbon.WorkingMemory.Agenda as A
import Data.List.Util
-- Haskel modules
import qualified Data.Set as S
import qualified Data.Map as M
import Data.Maybe
import Data.List
import Data.Hashable
import Data.Function
import Control.Monad
import Control.Monad.Identity
import Control.Monad.State.Lazy
import Control.Applicative((<*), (<$>), (<*>))

type Hash = Int
-------------------------------------------------------------------------
-- An API Data Types
-------------------------------------------------------------------------
data Automaton = Automaton {
    auSpNet         :: SpNet HSentence,
    auRuleIndex     :: I.MapOnSets (Rule HSentence),
    auSentenceCache :: S.Set HSentence,
    auSolutionCache :: O.MapOnSets ( (Hash,Hash), [ Word ]) VarBindings,
    auAgenda        :: A.Agenda HSentence,
    auReachGoals    :: S.Set (Rule HSentence)
    } deriving (Eq, Show)
--
instance Hashable Automaton where
    hashWithSalt l a = 
        let sents = auSentenceCache a
            sz    = S.size sents
            l'    = hashWithSalt l sz
        in S.foldl hashWithSalt l' sents
--
type AutomatonState = State Automaton
-------------------------------------------------------------------------
-- An API Functions
-------------------------------------------------------------------------
-- | Returns an empty Working Memory Automaton.
newAutomaton :: Automaton
newAutomaton = 
    Automaton{ auSpNet         = spnEmpty,
               auRuleIndex     = I.empty,
               auSentenceCache = S.empty,
               auSolutionCache = M.empty,
               auAgenda        = A.newAgenda,
               auReachGoals    = S.empty }
--
member :: HSentence -> Automaton -> Bool
member snt au =
    S.member snt $ auSentenceCache au
--
enlist :: Automaton -> [HSentence]
enlist au =
    S.toList $ auSentenceCache au
--
count  :: Automaton -> Int
count  au = 
    S.size  $ auSentenceCache au
--
agenda :: Automaton -> [Rule HSentence]
agenda = A.enlist . auAgenda
--
agendaHas :: HSentence -> Automaton -> Bool
agendaHas s = (A.hasSignature s) . auAgenda
--
agendaLookup :: HSentence -> Automaton -> Maybe (Rule HSentence)
agendaLookup s = (A.get s) . auAgenda
--
goals :: Automaton -> [Rule HSentence]
goals = S.toList . auReachGoals
-- | Apply sentence to the automaton and return (de)activated rules 
--   which may be an actions or goals.
handle :: Modify HSentence -> AutomatonState [Modify (Rule HSentence)]
handle ms = do
  all <- (apply ms >>= interpret)
  let (ifs:iffs:goals:acts:_)  = separate seps  all
  forM_ acts  updateAgenda
  forM_ goals updateGoal
  rs <- forM (iffs ++ goals ++ filter isAssert ifs) $ handle . (ruleSignature <$>)
  return $ goals ++ acts ++ concat rs
    where seps             = (. mdfArg) <$> [isIf,isIff,isGoal,isAction]
          updateAgenda mdr = do
              ag <- A.update mdr <$> gets auAgenda
              modify $ \au ->
                  au { auAgenda = ag }
          updateGoal   mdr = do
              let setOp = if isAssert mdr then S.insert else S.delete
              modify $ \au ->
                  au { auReachGoals = setOp (mdfArg mdr) $ auReachGoals au }

-------------------------------------------------------------------------
-- Private Functions
-------------------------------------------------------------------------
--
-- Apply one sentence to the automaton.
--
apply :: Modify HSentence -> AutomatonState [Result]
apply md = do
    spn      <- gets auSpNet
    solCache <- gets auSolutionCache
    senCache <- gets auSentenceCache
    let (results, 
         solCache') = (flip runState) solCache $ spnApply spn md
        modifyAutomaton sc = do modify $ \sol-> sol{auSentenceCache = sc, 
                                                    auSolutionCache = solCache' }
                                return results
    case md of
      Assert s | S.notMember s senCache ->
            modifyAutomaton $ S.insert s senCache
      Retire s | S.member    s senCache -> 
            modifyAutomaton $ S.delete s senCache
      _ -> 
            return []
--
-- Substitute solutions into rules.
--
interpret :: [Result] -> AutomatonState [Modify (Rule HSentence)]
interpret rs = do
  ruInd <- gets auRuleIndex
  return $ do r  <- rs
              let (h,vbs) = mdfArg r
                  rules   = S.toList $ I.lookup h ruInd
              vb   <- vbs
              rule <- evalArith <$> substitute vb rules
              guard $ evalGuard $ ruleGuard rule
              return $ mdfOp r rule 
--
-- Compile rule module.
--
compileModule :: (Eq s,
                  Ord s,
                  Substitutable s, 
                  Sentence s) => 
                 Module s -> AutomatonState ()
compileModule m0 = do
    let m        = unifyModule (fromSentence <$> m0)
        rulesSps = spFromList <$> ruleCondition <$> modRules m
        spn      = (flip execState) spnEmpty $ mapM_ spnAddPlan rulesSps
        au       = newAutomaton
    put $ au {auSpNet     = spn, 
              auRuleIndex = I.fromList $ zip (spId <$> rulesSps) $ modRules m}
--
-- Unify all variables of all rules of a module. This is a vital 
-- procedure to identify identical subplans (sub trees) of an SpNet.
--
unifyModule :: (Eq s,
                Substitutable s, 
                Sentence s) => 
               Module s -> Module s
unifyModule m = 
    (flip evalState) [] $ do
      let ruleRate      = length . ruleCondition
          rulesRated    = reverse $ sortOn ruleRate $ modRules m
      rules'  <- unifyVars ruleCondition rulesRated
      return $ m{ modRules = rules' }
--
type TextState s = State [s]
--
unifyVars :: (Substitutable a,
              Eq s,
              Sentence s) => 
              (a -> [s]) -> [a] -> TextState s [a]
unifyVars _ [] = return []
unifyVars getTxt (a:as) = do
  accTxt <- get
  let vbs = map swap $ maxSubstitution zipLikeStrategy accTxt $ getTxt a
      a'  = substitute vbs a
  put $ nub $ (getTxt a') ++ accTxt
  as' <- unifyVars getTxt as
  return $ a':as'
--
sortOn f = sortBy (compare `on` f)
--
swap (x,y) = (y,x)
