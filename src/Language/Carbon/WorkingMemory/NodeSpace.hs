{-# LANGUAGE MultiParamTypeClasses, 
             FunctionalDependencies,
             FlexibleInstances, 
             DeriveFunctor,
             TupleSections #-}
module Language.Carbon.WorkingMemory.NodeSpace
    (
     Node(..),
     NodeAddress,
     rootNodeFromModule,
     update
    ) where
------------------------------------------------------------------
--import Language.Carbon.WorkingMemory.Space
import qualified Language.Carbon.WorkingMemory.Automaton as A
import Language.Carbon.Model
import Language.Carbon.HSentence

import Data.Maybe
import Data.List
import Data.Function
import Data.Hashable
import qualified Data.Map as M
import Control.Monad.Trans.State.Lazy
import Control.Monad
import Control.Applicative((<*), (<$>), (<*>))

--import Debug.Trace
------------------------------------------------------------------
-- Data types
------------------------------------------------------------------
type NodeAddress = [HSentence]
--
data Node = 
    Node {
      address  :: NodeAddress,
      action   :: Rule HSentence,
      children :: M.Map HSentence Node,
      wmHash   :: Int,
      cache    :: A.Automaton
    } |
    NodeRef {
      address  :: NodeAddress
    } deriving Show
------------------------------------------------------------------
-- API functions
------------------------------------------------------------------
rootNodeFromModule :: Module HSentence -> Node
rootNodeFromModule m =
    rootNodeFromState $ execState (A.compileModule m) A.newAutomaton
------------------------------------------------------------------
-- Private functions
------------------------------------------------------------------
--
rootNodeFromState :: A.Automaton -> Node
rootNodeFromState a0 = 
    let act0    = Action { ruleSignature = fromWords [],
                           ruleCondition = [],
                           ruleGuard     = And [],
                           actionEffect  = [] }
    in nodeFromAction a0 [] act0
--
nodeFromAction :: A.Automaton -> NodeAddress -> Rule HSentence -> Node
nodeFromAction a0 pAddr r =
  let sig     = ruleSignature r
      a1      = execState (mapM_ A.handle $ actionEffect r) a0
      addr    = (sig:pAddr)
      agd     = A.agenda a1
      chNodes = nodeFromAction a1 addr <$> agd
  in Node{ address  = addr,
           action   = r,
           children = M.fromList (zip ((head . address) <$> chNodes) 
                                  chNodes),
           wmHash   = hash a1,
           cache    = a1}
--
-- Distructive update of a WM Space. This function drops out old s-node 
-- and creates a new one reusing only WM state (Automaton s).
--
--updateRootD :: [Modify s] -> Node s -> NodeSpaceState s (Node s)
--updateRootD ms Node{cache = a0} = 
--    rootNodeFromState $ execState (mapM_ A.handle ms) a0
--
-- Recursive update of WMs of s-node tree preserving its tree meta structure.
--
update :: [Modify HSentence] -> Node -> Node
update ms n@Node{address  = addr,
                      cache    = a0,
                      children = ch0} =
  let (events, a1)        = runState (concat <$> mapM A.handle ms) a0
      (actsSigsA, 
       actsSigsR)         = let (eventsA,  eventsR)  = mdfPartition events
                                actsSigs = (ruleSignature <$>) . 
                                           (filter isAction) . 
                                           (mdfArg <$>)
                            in (actsSigs eventsA, actsSigs eventsR)
      (sigsUpd1, sigsDel) = 
          partition (\s-> A.agendaHas s a1) actsSigsR
      (sigsUpd2, sigsNew) = 
          partition (\s-> A.agendaHas s a0) actsSigsA
      sigsUpd = sigsUpd1 ++ sigsUpd2
      ch1D = foldr M.delete ch0 sigsDel
      ch1U = foldl (\ch s ->
                        let n = fromJust $ M.lookup s ch
                            r = fromJust $ A.agendaLookup s a1
                            n' = update (actionEffect r) n{action = r, cache = a1}
                        in M.insert s n' ch) ch1D sigsUpd
      ch1 = foldl (\ch s ->
                       let r = fromJust $ A.agendaLookup s a1 
                           n = nodeFromAction a1 addr r
                       in M.insert s n ch) ch1U sigsNew
  in n{ wmHash   = hash a1,
        cache    = a1,
        children = ch1}
