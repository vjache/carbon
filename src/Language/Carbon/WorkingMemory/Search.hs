{-# LANGUAGE MultiParamTypeClasses, 
             FunctionalDependencies,
             FlexibleInstances, 
             DeriveFunctor,
             TupleSections,
             DeriveGeneric #-}
module Language.Carbon.WorkingMemory.Search 
    (
     PrioRange(..),
     Prio(..),
     SearchSession(..),
     VisitStatus(..),
     searchPrio,
     visitPrio,
     fetchGoals,
     fetchGoalsPerNode
    ) where

import GHC.Generics (Generic)

import qualified Language.Carbon.WorkingMemory.Automaton as A
import Language.Carbon.WorkingMemory.NodeSpace
import Language.Carbon.Model
import Language.Carbon.HSentence

import Data.Maybe
import Data.List
import qualified Data.Foldable as F
import qualified Data.Traversable as T
import qualified Data.PQueue.Prio.Min as Q
import qualified Data.PQueue.Prio.Max as QMX
import qualified Data.IntSet as IS
import qualified Data.Map as M
import Control.Monad
import Control.Applicative((<*), (<$>), (<*>))

import GHC.Exts

import Debug.Trace 

import Control.Parallel (par, pseq)
import Control.Parallel.Strategies
import Control.DeepSeq

-----------------------------------------------------------------------------------
-- Data Types & Classes
-----------------------------------------------------------------------------------

data PrioRange = Low  | Normal | High deriving (Eq, Ord, Show, Generic)
data Prio = PrioNever |
            PrioMin  | 
            PrioMax | 
            Prio {pRange :: PrioRange, pRank :: Int} deriving (Eq, Ord, Show, Generic)
instance NFData PrioRange
instance NFData Prio
-- Trinary status of checking the fact that we searched some 
-- state. Binary status, i.e boolean, is inconvenient in a context 
-- of a using Bloom Filter which may reliably answer that the 
-- element NOT contained by set. We may track a limited number of 
-- visited states for sure and evict them into Bloom-Filter when buffer 
-- is exceeds its limits.
data VisitStatus = NotVisited | 
                   SeemsVisited | 
                   Visited deriving (Show, Eq, Ord)
--
class Monad ss => SearchSession ss where
    enqueue      :: Prio -> Node -> ss ()
    dequeue      :: ss (Maybe Node)
    minPrio      :: ss Prio
    maxPrio      :: ss Prio
    memoVisited  :: Node -> ss ()
    checkVisited :: Node -> ss VisitStatus

-----------------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------------

--
searchPrio :: SearchSession ss => 
              (Node -> ss Prio) -> [Node] -> ss [Node]
searchPrio prio ns = do
  enqueueNodes prio ns
  searchP prio 
--
searchP :: SearchSession ss => 
           (Node -> ss Prio) -> ss [Node]
searchP prio = do
  mn <- dequeue
  trace "SEARCHING" $ case mn of
    Just n ->
        do v <- checkVisited n
           case v of
             NotVisited -> 
                  do memoVisited n
                     let cns = children n
                     enqueueNodes prio cns
                     cns'' <- searchP prio
                     return $ n:cns''
             SeemsVisited -> 
                  do enqueue PrioMin n
                     searchP prio
             Visited ->
                  searchP prio
    Nothing -> 
        return []
-------------------------------------
--
visitPrio :: SearchSession ss => (Node -> ss Prio) -> [Node] -> ss ()
visitPrio prio ns = do
  enqueueNodes prio ns
  visitP prio
--
visitP :: SearchSession ss => 
           (Node -> ss Prio) -> ss ()
visitP prio = do
  mn <- dequeue
  --trace "SEARCHING" $ 
  case mn of
    Just n ->
        do v <- checkVisited n
           case v of
             NotVisited -> 
                  do memoVisited n
                     enqueueNodes prio $ children n
                     visitP prio
             SeemsVisited -> 
                  do enqueue PrioMin n
                     visitP prio
             Visited ->
                  visitP prio
    Nothing -> 
        return ()
--
enqueueNodes prio = 
 T.mapM $ \n -> do p <- prio n
                   enqueue p n
--
pqueueFromList prio maxp q = 
    F.foldr (\n0 (maxp0, q0) ->
               let minp0 = maybe maxp0 fst $ Q.getMin q0
                   n0p   = prio minp0 maxp0 n0
                   maxp1 = max maxp0 n0p
               in (maxp1, Q.insert n0p n0 q0)) (maxp, q)
--
fetchGoals :: [Node] -> [(NodeAddress, Rule HSentence)]
fetchGoals ns = do 
  n <- ns
  (address n,) <$> (A.goals $ cache n)
  
fetchGoalsPerNode :: [Node] -> [(NodeAddress, [Rule HSentence])]
fetchGoalsPerNode ns = do 
  n <- ns
  return (address n, A.goals $ cache n)
