{-# LANGUAGE MultiParamTypeClasses, 
             FunctionalDependencies,
             FlexibleInstances, 
             DeriveFunctor #-}
{-
  An Agenda aggregator container logic.
 -}
module Language.Carbon.WorkingMemory.Agenda
    (
     Agenda(..),
     newAgenda,
     update,
     enlist,
     get,
     hasSignature
    ) where

-- Carbon modules
import Language.Carbon.Model
import qualified Data.MapOnSets as O
-- Haskel modules
import qualified Data.Set as S
import qualified Data.Map as M
import Data.List
import Control.Applicative((<*), (<$>), (<*>))

type Agenda s = O.MapOnSets s (Rule s) 

newAgenda :: Agenda s
newAgenda = M.empty
--
enlist :: Ord s => Agenda s -> [Rule s]
enlist ag = aggregate <$> S.toList <$> M.elems ag
--
get :: Ord s => s -> Agenda s -> Maybe (Rule s)
get s ag = case S.toList $ O.lookup s ag of
             []   -> Nothing 
             acts -> Just $ aggregate acts
hasSignature :: Ord s => s -> Agenda s -> Bool
hasSignature s = O.hasKey s
--
update :: (Eq s, Ord s) => Modify (Rule s) -> Agenda s -> Agenda s
update (Assert r1@Action{ruleSignature = sig}) = O.insert sig r1
update (Retire r1@Action{ruleSignature = sig}) = O.delete sig r1
--
aggregate :: (Eq s, Ord s) => [(Rule s)] -> (Rule s)
aggregate acts@(a:_) = 
      a{ ruleCondition = nub $ concatMap ruleCondition acts,
         ruleGuard     = And $ nub $ ruleGuard <$> acts,
         actionEffect  = nub $ concatMap actionEffect acts }
           
