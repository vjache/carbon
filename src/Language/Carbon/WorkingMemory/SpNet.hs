{-# LANGUAGE MultiParamTypeClasses, 
             FunctionalDependencies,
             FlexibleInstances, 
             DeriveFunctor #-}
{-
 - Solution Plans Network Automata.
 -}
module Language.Carbon.WorkingMemory.SpNet
    (
     SpNet,
     Result,
     spnEmpty,
     spnAddPlan,
     spnApply,
     toSpTree
    ) where

import Language.Carbon.Model
import Language.Carbon.WorkingMemory.SolutionPlan
import qualified Data.MapOnSets as O
import Control.Monad
import qualified Data.Set as S
import qualified Data.Map as M
import Data.Maybe
import Data.List
import Control.Monad.Identity
import Control.Monad.State.Lazy
import Control.Applicative((<*), (<$>), (<*>))
import Data.Hashable

import Data.Function

type Hash = Int
----------------------------------------------------------------------------
data SpNet s = SpNet { 
      spnIndex     :: M.Map Hash (SolutionPlan s),
      spnUpstream  :: O.MapOnSets Hash Hash     } deriving (Show, Eq)
type SpNetState s = State (SpNet s)
type Result       = Modify (Hash, [VarBindings])
type SpCacheState = State (O.MapOnSets ((Hash,Hash),[Word]) VarBindings)
----------------------------------------------------------------------------
spnEmpty :: SpNet s
spnEmpty = SpNet {spnIndex = M.empty, spnUpstream = M.empty}
--
spnInsert :: Sentence s =>  SolutionPlan s -> SpNetState s ()
spnInsert sp@SpJoin{spId = id, spLeft = l, spRight = r} = do
    let insSp = M.insert id sp{spLeft = SpRef $ spId l, spRight = SpRef $ spId r}
        insUp h = O.insert h id 
    spn <- get 
    put spn{spnIndex    = insSp $ spnIndex spn,
            spnUpstream = insUp (spId l) $ insUp (spId r) $ spnUpstream spn}
spnInsert sp@SpTerm{spId = id} = do
    let insSp = M.insert id sp
    spn <- get 
    put spn{spnIndex    = insSp $ spnIndex spn,
            spnUpstream = O.insert (spId spnBottomSp) id $ spnUpstream spn}
--
spnEnlistUpstreamSps ::  SpNet s -> Hash -> [SolutionPlan s]
spnEnlistUpstreamSps spn spid = 
    spnGetSp spn <$> (S.toList $ O.lookup spid $ spnUpstream spn)
--
spnGetSp :: SpNet s -> Hash -> SolutionPlan s
spnGetSp spn id = 
    fromJust $ M.lookup id $ spnIndex spn
--
spnBottomSp = spRef $ hash (0 :: Int)
--
--
toSpTree :: Sentence s => SpNet s -> Hash -> SolutionPlan s
toSpTree spn h = 
    case spnGetSp spn h of
      sp@SpTerm{} -> sp
      sp@SpJoin{spLeft = l, spRight = r} -> 
          sp{spLeft  = toSpTree spn $ spId l, 
             spRight = toSpTree spn $ spId r}
--
spnAddPlan :: (Ord s, Sentence s) => SolutionPlan s -> SpNetState s ()
spnAddPlan sp = do
  msp <- gets $ (M.lookup $ spId sp) . spnIndex
  case msp of
    Nothing -> do
        spnInsert sp
        mapM_ spnAddPlan $ spChildren sp
    Just _ ->
        return ()
-------------------------------------------------------------------------------
-- TODO: Use (Modify s) instead of just (s)
spnApply :: Sentence s => SpNet s -> Modify s -> SpCacheState [Result]
spnApply spn mdf = do
  let termSps      = spnEnlistUpstreamSps spn $ spId spnBottomSp
      matchedSps = filter (isJust . snd) $ zip termSps $ 
            (spnMatchPattern $ Just s) <$> spSentence <$> termSps
  concat <$> sequence [let mdfVb = toMdf $ fromJust mvb 
                       in spnPropagateSolution spn mdfVb spnBottomSp toSp | 
                       (toSp, mvb) <- matchedSps]
      where s       = mdfArg mdf
            toMdf a = mdf{ mdfArg = a }
--
-- Propagate solution (VarBindings) from one solution plan to another.
--
spnPropagateSolution :: Sentence s => 
                SpNet s -> 
                Modify VarBindings -> 
                SolutionPlan s -> 
                SolutionPlan s -> SpCacheState [Result]
spnPropagateSolution spn 
             mdfVb 
             fromSp 
             toSp 
    -- SP Join case
    | spIsJoin toSp = 
        let 
            SpJoin{ spId       = toSpId,
                    spJoinVars = jvars, 
                    spLeft     = l, 
                    spRight    = r} = toSp
            (readCache,
             prod)     = if spId l == spId fromSp
                         then ((spId r, toSpId), lprod)
                         else ((spId l, toSpId), flip lprod)
            key        = fetch vb <$> jvars
            writeCache = (spId fromSp, spId toSp)
        in do
          modify $ cacheOper (writeCache, key) vb
          producedSols <- gets $ prod [vb] . O.enlistValuesFor (readCache,key)
          if null producedSols
          then return []
          else do
            upsResults <- concat <$> mapM propagateUps producedSols
            return $ (mdfOp (toSpId, producedSols)):upsResults
    -- SP Term case
    | spIsTerm toSp = 
        let SpTerm{spId = toSpId, spVars = vars } = toSp
            vbVars = fst <$> vb
            presentInSp = (flip elem) vbVars
        in if all presentInSp vars
           then do
             upsResults <- propagateUps vb
             return $ (mdfOp (toSpId, [vb])):upsResults
           else return []
    where
      vb        = mdfArg mdfVb
      (cacheOper, 
       mdfOp)   = if isAssert mdfVb 
                  then (O.insert, Assert)
                  else (O.delete, Retire)
      fetch l k = fromJust $ lookup k l
      lprod :: Eq a => [[a]] -> [[a]] -> [[a]]
      lprod lset rset = [ nub $ x ++ y | x <- lset, y <- rset ]
      -- Propagate to upstream solution plans
      propagateUps vb' =
          let ups = spnEnlistUpstreamSps spn (spId toSp)
          in  concat <$> (forM ups $ spnPropagateSolution spn (mdfOp vb') toSp)
----------------------------------------------------------------------------
-- Match pattern against sentence and maybe return a var bindings.
--
-- The second argument is considered to be a pattern i.e. sentence 
-- with vars while the first one is consedered to be a concrete 
-- sentence with non var words.
spnMatchPattern :: (MonadPlus m, Sentence s) => m s -> s -> m VarBindings
spnMatchPattern ms p = do 
  s <- ms
  res <- match (toWords p) (toWords s)
  guard $ check res
  return res
    where match [] [] = return []
          match [] _  = mzero
          match _ []  = mzero
          match (pw:pt) (sw:st) = do
            rest <- match pt st
            let bind = (pw, sw)
            case pw of
              (Var d _) | domainOf sw == d -> return $ bind:rest
              _         | pw == sw         -> return rest
                        | otherwise        -> mzero
          check [] = True
          check ((var, val):bs) =
            maybe (check bs) (\val1 -> 
                                  if   val /= val1 
                                  then False
                                  else check bs) $ lookup var bs
---------------------------------------------------------------------------
-- TESTs
---------------------------------------------------------------------------
evalTest t = evalState t M.empty
runTest  t = runState  t M.empty
execTest t = execState t M.empty

-- | Test Plan:
-- 1. Create Solution Plan
-- 2. Build SP Net
-- 3. Apply sentences

test0 = 
    let p = [Var STR "name", Key "nameOf", Var INT "id"] :: [Word]
        s = [S "John",   Key "nameOf",    I 0]
    in spnMatchPattern (Just s) p
--
spnApplyAll spn mdfL = mapM (spnApply spn) mdfL
--
test1 :: SpCacheState [[Result]]
test1 = do
  let sp = spFromList [[Var STR "name",   Key "nameOf",   Var INT "id"],
                       [Var STR "family", Key "familyOf", Var INT "id"],
                       [Var INT "id",     Key "friendOf", Var INT "id1"]
                      ]
      spn = execState (spnAddPlan sp) spnEmpty
  spnApplyAll spn [Assert [S "John",    Key "nameOf",    I 0],
                   Assert [S "Ramboo",  Key "familyOf",  I 0],
                   Assert [I 0,         Key "friendOf",  I 1]]
  --spnApply spn [I 0,         Key "friendOf",  I 1]
------------------------------------------------------------------------------------------
{-
  Plan
  * Adopt (Modify s) by SpNet.spnApply
  * Develop spnMergePlanInto. It is an advanced version of spnAddPlan which 
  performs also variables unification before add.
  * Guards for SolutionPlan
  * Module Compiler =  Model/Action + SolutionPlan + SpNet + VarUnify = Build Optimal SpNet
 -}
