{-# LANGUAGE MultiParamTypeClasses, 
             FunctionalDependencies,
             FlexibleInstances, 
             DeriveFunctor #-}

module Language.Carbon.WorkingMemory.Space 
    (
     Space(..), 
     ActionEvent(..), 
     GoalEvent(..), 
     Event(..)
    ) where

import Language.Carbon.Model
import Control.Monad.State.Lazy
import Control.Monad
------------------------------------------------------------------
data ActionEvent vsn s = ActionAvailable (Rule s, vsn) |
                         ActionUnavailable (Rule s, vsn)
data GoalEvent vsn s   = GoalReachable (Rule s, vsn) | GoalUnreachable (Rule s, vsn)
data Event vsn s       = AEvent (ActionEvent vsn s)| 
                         GEvent (GoalEvent vsn s)
-- V2
class (Monad w, Ord vsn) => Space w vsn | w -> vsn where
    fromModule        :: Sentence s => Module s -> w ()
    agenda            :: Sentence s =>             w [(Rule s, vsn)]
    legenda           :: Sentence s =>             w [(Rule s, vsn)]
    focus             ::                    vsn -> w ()
    rootVsn           ::                           w vsn
    vsn               ::                           w vsn
    distance          ::             vsn -> vsn -> w Int
    shortestPath      ::             vsn -> vsn -> w [Rule s]
    goals             ::                           w [Rule s]
    -- Destructive ops
    -- Change current state applying a bunch of modifications, i.e. asserting 
    -- and retireing sentences. This lead to partioal (more or less) recomputation 
    -- of a state versions.
    update            :: Sentence s => [Modify s] -> w [Event vsn s]
    -- Change current state (root vsn) choosing specified action signature.
    -- This may also be achived by using 'update' with action effect 
    -- but it would lead to recompute of a state versions which are already 
    -- computed. So, it is much more effecient just to choose already 
    -- computed version of state.
    move              :: Sentence s =>          s -> w [Event vsn s]
