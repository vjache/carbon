{-# LANGUAGE  FlexibleInstances, TupleSections #-}
module Language.Carbon.WorkingMemory.Search.Concurrent
    (
     runSearch
    ) where

import Language.Carbon.WorkingMemory.Search
import Language.Carbon.WorkingMemory.NodeSpace
import Language.Carbon.Model
import Language.Carbon.HSentence
import Language.Carbon.PP

import qualified Data.PQueue.Prio.Max as QMX
import qualified Data.IntSet as IS
import Data.List
import Data.Maybe
import Control.Monad
import Control.Applicative((<*), (<$>), (<*>))
import Control.Concurrent
import Control.Concurrent.STM
import qualified Control.Concurrent.MSem as MSem

import Control.Monad.Trans.Reader
import Control.Monad.Trans.Class
import Control.DeepSeq
import Control.Exception

import Prelude hiding (read)

import Debug.Trace
import GHC.Conc (labelThread)
-----------------------------------------------------------------------------------
-- API Functions
-----------------------------------------------------------------------------------
--
runSearch distPrio n = do
  -- Get number of CPUs
  cap <- getNumCapabilities
  putStrLn $ show cap
  -- Set up task blocking queue size
  let resQSize  = 30 * cap
  sr  <- newSearchIO resQSize
  -- Put first s-node
  eventIO "Put first s-node" $ runSearchIO (do p <- distPrio n
                                               enqueue p n) sr
  -- Start search workers
  wts <- forM [1..cap] $ \ k -> forkFinally (eventIO "runWork" $ runWork sr k) stdFinal
  return (srResultQueue sr, stopThreads wts)
      where 
        -- | Cyclic poll of task queue and visiting s-node's
        runWork :: Search -> Int ->IO ()
        runWork sr k = do
          tid <- myThreadId
          labelThread tid $ "WRK-" ++ show k
          runSearchIO (visitPrio distPrio []) sr
        -- |
        stdFinal e = putStrLn $ "FINISHED: " ++ (show e)
        -- | Stop pump & workers threads
        stopThreads ts = mapM_ killThread ts
--
eventIO :: String -> IO a -> IO a
eventIO label =
  bracket_ (traceEventIO $ "START " ++ label)
           (traceEventIO $ "STOP "  ++ label)
-----------------------------------------------------------------------------------
-- Data Types & Classes
-----------------------------------------------------------------------------------
--
data Search = Search {srVisited     :: TVar IS.IntSet,
                      srPrioQueue   :: TVar (QMX.MaxPQueue Prio Node),
                      srPrioQueueSem:: MSem.MSem Int,
                      srResultQueue :: TBQueue (NodeAddress, [Rule HSentence]),
                      srMinP        :: TVar Prio}
--
type SearchSessionSTM = ReaderT Search STM
--
type SearchSessionIO  = ReaderT Search IO
--
instance SearchSession SearchSessionIO where
    enqueue p0 n   = do atomic $ enqueue p0 n
                        sem <- asks srPrioQueueSem
                        evalTop
                        (lift . MSem.signal) sem
                        return ()
        where evalTop = do tq <- asks srPrioQueue
                           q <- (lift . atomically) $ readTVar tq
                           case QMX.getMax q of
                             Just (p, n') -> 
                                 do
                                   let r = fetchGoalsPerNode [n']
                                       h = wmHash n'
                                   (lift . evaluate) $ force $ (p, r, h)
                                   return ()
                             Nothing ->
                                 return ()
    dequeue        = do 
      tq   <- asks srPrioQueue
      sem <- asks srPrioQueueSem
      (lift . MSem.wait) sem
      -- 1. Take a node with most priority
      n <- (lift. atomically) $ do 
                       (n', q') <- (fromJust . QMX.maxView) <$> readTVar tq
                       writeTVar tq q'
                       return n'
      return $ Just n
    minPrio        = atomic minPrio
    maxPrio        = atomic maxPrio
    memoVisited n  = do 
      gpn <- atomic $ do
                       -- Put goals reached by current node 'n' into results queue
                       let gpn' = filter ((/=[]) . snd) $ fetchGoalsPerNode [n]
                       rq <- asks srResultQueue
                       forM_ gpn' $ writeTBQueueL rq
                       -- Mark current node as 'visited'
                       modify srVisited (IS.insert (wmHash n))
                       return gpn'
      (lift . evaluate) $ force gpn
      return ()
    checkVisited n = atomic $ checkVisited n
--
instance SearchSession SearchSessionSTM where
    --
    enqueue p0 n = do
      let insQueue p = modify srPrioQueue (QMX.insert p n)
      case p0 of
           PrioNever ->
                return ()
           PrioMin -> do
                minp <- minPrio
                let p = minp{pRank = (pRank minp) - 1}
                write srMinP p
                insQueue p
           PrioMax -> do
                maxp <- maxPrio
                let p = maxp{pRank = (pRank maxp) + 1}
                insQueue p
           Prio{} -> do
                insQueue p0
                modify srMinP  (min p0)
    --
    dequeue = return Nothing --asks srTaskQueue >>= readTBQueueL >>= (return . Just)
    --
    minPrio = read srMinP
    --
    maxPrio = do
      mp <- QMX.getMax <$> read srPrioQueue
      return $ maybe (Prio Normal 0) fst mp
    --
    memoVisited n = do
      -- Put goals reached by current node 'n' into results queue
      let goals = fetchGoalsPerNode [n]
      rq <- asks srResultQueue
      forM_ goals $ \r@(_, gs) -> when (gs /= []) $ writeTBQueueL rq r
      -- Mark current node as 'visited'
      modify srVisited (IS.insert (wmHash n))
    --
    checkVisited n = do b <- IS.notMember (wmHash n) <$> read srVisited
                        return $ if b 
                                 then NotVisited
                                 else Visited
-----------------------------------------------------------------------------
-- Private helper functions
-----------------------------------------------------------------------------
--
runSearchIO :: SearchSessionIO a -> Search -> IO a
runSearchIO sio sr = runReaderT sio sr
--
runSearchSTM :: SearchSessionSTM a -> Search -> STM a
runSearchSTM sstm sr = runReaderT sstm sr
--
newSearchIO :: Int -> IO Search
newSearchIO resultQueueSize = do 
  tvis  <- newTVarIO IS.empty
  tpq   <- newTVarIO QMX.empty
  pqs   <- MSem.new 0
  trq   <- newTBQueueIO resultQueueSize
  tprio <- newTVarIO $ Prio Normal 0
  return Search {srVisited     = tvis,
                 srPrioQueue   = tpq,
                 srPrioQueueSem= pqs,
                 srResultQueue = trq,
                 srMinP        = tprio}
--
atomicallyL :: STM a -> SearchSessionIO a
atomicallyL = lift . atomically
--
atomic :: SearchSessionSTM a -> SearchSessionIO a
atomic stm = do
      sr <- ask
      atomicallyL $ runSearchSTM stm sr
--
atomic_ stm  = atomic stm >> return ()
--
readTVarL :: TVar a -> SearchSessionSTM a
readTVarL     = lift . readTVar
--
writeTVarL :: TVar a -> a -> SearchSessionSTM ()
writeTVarL v  = lift . (writeTVar v)
--
modifyTVarL :: TVar a -> (a -> a) -> SearchSessionSTM ()
modifyTVarL v = lift . (modifyTVar v)
--
write prop val = do
  tvar <- asks prop
  writeTVarL tvar val
--
read prop = do
  tvar <- asks prop
  readTVarL tvar
--
modify prop f = do 
  tvar <- asks prop
  modifyTVarL tvar f
--
writeTQueueL :: TQueue a -> a -> SearchSessionSTM ()
writeTQueueL tq = lift . (writeTQueue tq)
--
readTQueueL :: TQueue a -> SearchSessionSTM a
readTQueueL = lift . readTQueue
--
writeTBQueueL :: TBQueue a -> a -> SearchSessionSTM ()
writeTBQueueL tq = lift . (writeTBQueue tq)
--
readTBQueueL :: TBQueue a -> SearchSessionSTM a
readTBQueueL = lift . readTBQueue
