{-# LANGUAGE  FlexibleInstances #-}
module Language.Carbon.WorkingMemory.Search.Pure
    (
     Search,
     evalSearch,
     runSearch,
     emptySearch
    ) where

import Language.Carbon.WorkingMemory.Search
import Language.Carbon.WorkingMemory.NodeSpace
import Language.Carbon.Model

import qualified Data.PQueue.Prio.Max as QMX
import qualified Data.IntSet as IS
import Control.Monad.Trans.State.Lazy

-----------------------------------------------------------------------------------
-- Data Types & Classes
-----------------------------------------------------------------------------------

--
data Search = Search {srVisited :: IS.IntSet,
                      srQueue   :: QMX.MaxPQueue Prio Node,
                      srMinP    :: Prio}
--
instance SearchSession (State Search) where
    --
    maxPrio = do 
      mp <- gets $ QMX.getMax . srQueue
      return $ maybe (Prio Normal 0) fst mp
    --
    minPrio = gets $ srMinP
    --
    enqueue p0 n =
      let insert p = modify $ \sr -> sr{srQueue = QMX.insert p n $ srQueue sr,
                                        srMinP  = min p $ srMinP sr}
      in case p0 of
           PrioNever ->
               return ()
           PrioMin -> do
                   minp <- minPrio
                   insert minp{pRank = (pRank minp) - 1}
           PrioMax -> do
                   maxp <- maxPrio
                   insert maxp{pRank = (pRank maxp) + 1}
           Prio{} -> insert p0
    --
    dequeue = do
      q <- gets srQueue
      case QMX.maxView q of
        Just (n, q') -> do
                    modify $ \sr -> 
                        if QMX.null q'
                        then sr{srQueue = q',
                                srMinP  = Prio Normal 0}
                        else sr{srQueue = q'}
                    return $ Just n
        Nothing ->
            return Nothing
    --
    memoVisited n = 
      modify $ \sr -> sr{srVisited = IS.insert (wmHash n) $ srVisited sr}
    --
    checkVisited n = do b <- gets $ (IS.notMember (wmHash n)) . srVisited
                        return $ if b 
                                 then NotVisited
                                 else Visited
-----------------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------------
--
evalSearch :: State Search a -> a
evalSearch ss = evalState ss emptySearch
--
runSearch :: State Search a -> (a, Search)
runSearch ss = runState ss emptySearch
--
emptySearch :: Search
emptySearch = Search {srVisited = IS.empty,
                      srQueue   = QMX.empty,
                      srMinP    = Prio Normal 0}
