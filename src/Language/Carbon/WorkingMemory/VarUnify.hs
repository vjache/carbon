{- | 
   This module implements an algorithms concerning the problem 
   of a varible unification of two texts. A text is a sequence 
   of sentences, where sentence is a sequence of words, where 
   word is a constant or a variable. 
   The problem defined as follows:
     Let we have a two texts called 'left text' and 'right text'. We 
     want to find a max substitution of variables in a right text to the 
     variables in a left text such that the 'join relationships' would 
     not change between each pair of sentences in a right text in case 
     of substitution would be applied.
   The 'join relationship' between two sentences is a common variables 
   at the specific positions. For instance let we have two sentences of 
   the same text: 
    * (const1 ?var1 ?var2), 
    * (const2 const3 ?var1 ?var3 ?var4)
   where 'join relationship' apply constraint that two possible sentences 
   matching the correspondent patterns also must have the same value
   at the positions holding by var '?var1' while other variables may be bound 
   to non equal values.
   The possible variable substitutions which keep the former 'join relationship' 
   might be like:
    * [(?var1, ?v1)]
    * [(?var1, ?v1), (?var2, ?v2)]
   From the other hand, substitution like:
    * [(?var1, ?v1), (?var2, ?var4)]
   would broke the former 'join relationship' due to additional 
   join constraint appears after substitution, lets see:
    * (const1 ?v1 ?var4), 
    * (const2 const3 ?v1 ?var3 ?var4)
   junction will occur now against '?v1' and '?var4'.
 -}
module Language.Carbon.WorkingMemory.VarUnify
    (
     maxSubstitution,
     zipStrategy,
     zipLikeStrategy
    ) where

import Language.Carbon.Model
import Control.Monad
import Data.Maybe
import Data.List
import Data.Function
import Control.Applicative((<*), (<$>), (<*>))
------------------------------------------------------------------------------------
--type VarBindings = [(Word,Word)]
type StdText s = [s]
type PermuteStrategy s = StdText s -> StdText s -> [ [(s, s)] ]
--
zipStrategy :: PermuteStrategy s
zipStrategy t1 t2 = [zip t1 t2]
--
zipLikeStrategy :: Sentence s => PermuteStrategy s
zipLikeStrategy t1 t2 = 
    zipLikeStrategyWith shallowMatch t1 <$> permutations t2
    where shallowMatch s1 s2 = (varsArity s1 == varsArity s2) && 
                               (arity s1     == arity s2)
--
zipLikeStrategyWith :: Sentence s => (s -> s -> Bool) -> StdText s -> StdText s -> [(s,s)]
zipLikeStrategyWith _ [] _ = []
zipLikeStrategyWith _ _ [] = []
zipLikeStrategyWith shallowMatch (s1:t1) (s2:t2) =
    if shallowMatch s1 s2
    then (s1,s2):zipLikeStrategyWith shallowMatch t1 t2
    else zipLikeStrategyWith shallowMatch t1 (s2:t2)
--
maxSubstitution :: Sentence s => 
                   PermuteStrategy s -> StdText s -> StdText s -> VarBindings
maxSubstitution strat t1 t2 = 
    maximumBy (compare `on` length) $ match <$> strat t1 t2
--
match :: Sentence s => [ (s, s) ] -> VarBindings
match m = foldr matchCouple [] m
    where matchCouple (s1, s2) mx =
              maybe mx id $ matchTwoSentences (toWords s1) (toWords s2) mx
--           
matchTwoSentences :: [Word] -> [Word] -> [(Word, Word)] -> Maybe [(Word, Word)]
matchTwoSentences ws1 ws2 mx | ws1 == [] && ws2 == [] = Just mx
                             | ws1 == []              = Nothing
                             | ws2 == []              = Nothing
matchTwoSentences (w1:ws1) (w2:ws2) mx = do
  matchTwoSentences ws1 ws2 mx >>= matchTwoWords w1 w2
--
matchTwoWords :: Word -> Word -> [(Word, Word)] -> Maybe [(Word, Word)]
matchTwoWords w1 w2 mx | isVar w1 && 
                         isVar w2 && 
                         (domainOf w1 == domainOf w2) = 
                             case [b | b <- mx, 
                                            (fst b == w1 || 
                                             snd b == w2)] of
                             [(w1',w2')] | w1' == w1 && 
                                           w2' == w2 -> Just mx
                             []                      -> Just $ (w1,w2):mx
                             _                       -> Nothing
                       | w1 == w2   = Just mx
                       | otherwise  = Nothing

test6 = matchTwoSentences 
        [Var STR "v1", Key "familyOf", Var STR "v11", Var STR "v111"]
        [Var STR "v2", Key "familyOf", Var STR "v22", Var STR "v222"]
        []

test7 = let t1 = [ 
                   [Var STR "n", Key "nameOf",   Var INT "personId"],
                   [Var STR "f", Key "familyOf", Var INT "personId"],
                   [Var STR "f", Key "familyOf", Var INT "personId0"] ] :: [[Word]]
            t2 = [ [Var STR "f1", Key "familyOf", Var INT "personId1"],
                   [Var STR "f1", Key "familyOf", Var INT "personId2"],
                   [Var STR "n1", Key "nameOf",   Var INT "personId1"],
                   [Key "human", Var INT "personId1"] ]
        in maxSubstitution zipLikeStrategy t1 t2
