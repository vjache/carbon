{-# LANGUAGE MultiParamTypeClasses, 
             FunctionalDependencies,
             FlexibleInstances,
             DeriveGeneric #-}
module Language.Carbon.Search.SearchDriver
    (
     SearchDriver(..),
     Visitor(..),
     PrioRange(..),
     Prio(..),
     VisitStatus(..),
     MaxRadius
    ) where

import Language.Carbon.Model
import Language.Carbon.Search.NodeSpace

import Control.DeepSeq
import Control.Monad.Trans.Class

import GHC.Generics (Generic)

------------------------------------------------------------------------------------------
-- Data Types & Classes
------------------------------------------------------------------------------------------
-- 
data PrioRange = Low  | Normal | High deriving (Eq, Ord, Show, Generic)
-- 
data Prio = PrioNever |
            PrioMin  | 
            PrioMax | 
            Prio {pRange :: PrioRange, pRank :: Int} deriving (Eq, Ord, Show, Generic)
--
data VisitorDecision a = Continue {vdVisitor :: a} | 
                         Stop     {vdVisitor :: a} deriving (Show, Eq)
--
class  Visitor v where
    onVisit :: SearchDriver sd n s m => v -> n -> sd m (VisitorDecision v)
-- Trinary status of checking the fact that we searched some 
-- state. Binary status, i.e boolean, is inconvenient in a context 
-- of a using Bloom Filter which may reliably answer that the 
-- element NOT contained by set. We may track a limited number of 
-- visited states for sure and evict them into Bloom-Filter when buffer 
-- is exceeds its limits.
data VisitStatus = NotVisited | 
                   SeemsVisited | 
                   Visited deriving (Show, Eq, Ord)
-- Данный класс описывает драйвер поиска в пространстве состояний (узлов). Такой 
-- драйвер можно рассматривать как контекст времени выполнения поиска, который на 
-- время поиска "оборачивает" пространство узлов. Этот контекст в некотором виде 
-- содержит поисковую очередь и некую дополнительную "память" о том какие узлы 
-- уже посещались.
--
-- Определение класса устанавливает отношение между 4-мя классами, среди которых 
-- 'sd', по сути, и есть добавочный контекст, который есть монад-трансформер и 
-- трансформирует монаду(контекст) в котором живут узлы пространства состояний.
--
class (MonadTrans sd,
       NodeSpace n s m,
       Monad (sd m)) => SearchDriver sd n s m where
    -- Поместить узел, с учетом приоритета, в поисковую очередь.
    enqueue      :: Prio -> n -> sd m ()
    -- Взять узел с наибольшим приоритетом из поисковой очереди.
    dequeue      :: sd m (Maybe (Prio, n))
    -- Текущий минимальный приоритет используемый в поисковой очереди.
    minPrio      :: sd m Prio
    -- Текущий максимальный приоритет используемый в поисковой очереди.
    maxPrio      :: sd m Prio
    -- Запомнить/пометить данный узел как посещённый.
    memoVisited  :: n -> sd m ()
    -- Проверить является ли данный узел посещённым.
    -- Замечание: 
    --  Типом возвращаемого значения является расширенный 
    --  вариант булевого типа, где значения имеют следующую семантику:
    --   * NotVisited   - не посещенный
    --   * Visited      - посещенный
    --   * SeemsVisited - возможно посещенный
    --  Последнее значение дает возможность использовать например блум-фильтр 
    --  корректным образом. Блум-фильтр дает лишь точный ответ когда элемент НЕ 
    --  содержится в множестве. Поэтому в случае когда блум-фильтр дает ответ 
    --  ВОЗМОЖНО содержится, строго говоря, мы должны не выкидывать из рассмотрения 
    --  такой узел, а просто понижать его приоритет при поиске (например до 
    --  минимального). Тем самым мы в первую очередь посетим те узлы которые точно 
    --  НЕ посещались и в последнюю очередь те что уже возможно посещались.
    checkVisited :: n -> sd m VisitStatus
    -- Выполнить обход пространства состояний (пространства узлов) используя 
    -- функцию приоритета. Сбор результатов обхода может производиться как в 
    -- передаваемом посетителе (Visitor.onVisit), так и на уровне реализации 
    -- сессии, т.е. в функции 'memoVisited'.
    traverse :: Visitor v => v -> (n -> sd m Prio) -> sd m v
    -- Дефолтная реализация обхода пространства состояний.
    traverse vis prio = do
      -- 1. Взять наиболее приоритетный узел из очереди поиска.
      mn <- dequeue
      case mn of
        -- 2а. Если очередь не пуста то ...
        Just (p, n) ->
              do let visit = do
                          -- V0. Обновляем состояние визитора
                          vd <- onVisit vis n
                          -- V1. Фиксируем факт посщения узла 'n'
                          memoVisited n
                          -- V2. Помещаем в поисковую очередь все дочерние узлы узла 'n'.
                          --     Заметка: использование тут функции 'lift' обеспечивается
                          --              тем что sd есть трансформатор монад.
                          (lift . nodeChildren) n >>= enqueueNodes
                          -- V3. Если визитор решает ...
                          case vd of
                            -- V3.а. ... остановить обход, то вернуть последнее 
                            --       состояние визитора.
                            Stop vis1 ->
                                return vis1
                            -- V3.б. ... продолжить обход, то продолжить обход с 
                            --       последним состоянием визитора.
                            Continue vis1 ->
                                traverse vis1 prio
                 -- 3. Взять статус посещения узла 'n'
                 v <- checkVisited n
                 case v of
                   -- 3.1. Если узел НЕ посещен то посетить.
                   NotVisited            -> visit
                   -- 3.2. Если узел ВОЗМОЖНО посещен то
                   SeemsVisited 
                       -- 3.2а. Если приоритет узла 'n' был в диапазоне 'Low' 
                       --       то посетить(возможно ещё раз).
                       | pRange p == Low -> visit
                       -- 3.2б. Если приоритет узла 'n' был в диапазоне High | Normal 
                       --       отложить посещение, т.е. поместить узел обратно в 
                       --       поисковую очередь с самым низким приоритетом и 
                       --       продолжить обход.
                       | otherwise       -> enqueue PrioMin n >> traverse vis prio
                   -- 3.6. Если узел посещён то продолжить обход.
                   Visited               -> traverse vis prio
        -- 2б. Если очередь пуста то остановить обход.
        Nothing -> 
            return vis
        where enqueueNodes = 
                  mapM $ \n -> do p <- prio n
                                  enqueue p n
------------------------------------------------------------------------------------------
-- Простейший визитор ограничивающий поиск по количеству посещённых узлов.
instance Visitor Int where
    onVisit k _ | k == 0    = return $ Stop 0
                | otherwise = return $ Continue $ k - 1
--
newtype MaxRadius = MaxRadius Int deriving (Show, Eq)
-- Простейший визитор ограничивающий поиск по радиусу поиска.
instance Visitor MaxRadius where
    onVisit mr@(MaxRadius r) n = 
        do addr <- (lift . nodeAddress) n
           return $ if length addr > r
                    then Stop mr
                    else Continue mr
--
instance NFData PrioRange
instance NFData Prio
