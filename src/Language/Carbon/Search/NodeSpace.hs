{-# LANGUAGE MultiParamTypeClasses, 
             FunctionalDependencies,
             FlexibleInstances #-}
module Language.Carbon.Search.NodeSpace
    (
     NodeSpace(..)
    ) where

import Language.Carbon.Model
import Language.Carbon.HSentence

import Control.Monad
import Control.Applicative((<*), (<$>), (<*>))
import Control.Monad.Identity

import Control.Monad.State.Lazy
import Data.Map

-----------------------------------------------------------------------------------
-- Data Types & Classes
-----------------------------------------------------------------------------------
--
class (Sentence s, Monad m) => NodeSpace n s m | n -> s where
    -- Создать новый корневой пустой узел.
    nodeRoot     :: Module s -> m n
    -- Создать новый узел применив указанные изменения к данному.
    nodeUpdate   :: [Modify s] -> n -> m n
    -- Адрес узла в терминах сигнатур действий.
    nodeAddress  :: n -> m [s]
    -- Действие породившее узел.
    nodeAction   :: n -> m (Rule s)
    -- Дочерние узлы порождённые агендой данного узла.
    nodeChildren :: n -> m [n]
    -- Быстрый доступ к дочернему узлу по сигнатуре действия.
    nodeChild    :: s -> n -> m n
    -- Хэш состояния данного узла.
    nodeHash     :: n -> m Int
    -- Цели удовлетворенные в данном узле
    nodeGoals    :: n -> m [Rule s]
-----------------------------------------------------------------------------------------
-- Examples
-----------------------------------------------------------------------------------------
{-
data Node = Node {}
-- Не зависящий от природы монады экземпляр Пространства Узлов.
instance Monad m => NodeSpace Node HSentence m where
    nodeRoot m = 
        return Node{}
    nodeUpdate mdfs n =
        return n
    nodeAddress n =
        return []
    nodeAction n = 
        return Action{}
    nodeChildren n =
        return []
    nodeChild s n = 
        return n
    nodeHash n = 
        return 0
    nodeGoals n = 
        return []
--
instance Monad m => NodeSpace Int HSentence (StateT (Map Int [Int]) m) where
-}
