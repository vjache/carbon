
{-# LANGUAGE DeriveGeneric #-}
module Language.Carbon.HSentence 
    (
     HSentence,
     fromWords,
     fromSentence
    ) where

import Language.Carbon.Model
import Data.Hashable
import Control.DeepSeq
--
data HSentence = HSentence { hsHash  :: Int, 
                             hsWords :: [Word]} 
                 deriving (Show, Ord, Eq)
--
instance NFData HSentence
--
instance Hashable HSentence where
    hash = hsHash
    hashWithSalt l hs = 
        hashWithSalt l $ hsHash hs
--
instance Sentence HSentence where
    toWords = hsWords
--
instance Substitutable HSentence where
    substitute sl hs = fromWords $ substitute sl $ hsWords hs
--
instance ArithEvaluable HSentence where
    evalArith hs = fromWords $ evalArith $ hsWords hs
-----------------------------------------------------------------
fromWords :: [Word] -> HSentence
fromWords ws = HSentence { hsHash = hashWithSalt 111222333 ws, hsWords = ws}

fromSentence :: Sentence s => s -> HSentence
fromSentence = fromWords . toWords
