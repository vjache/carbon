module Language.Carbon.PP 
    (
     PP,
     putPpLn,
     pp,
     ppNl,
     ppShow,
     ppAddTab, 
     ppSubTab,
     ppExec,
     ppWord,
     ppSent, 
     ppMSent, 
     ppSents, 
     ppSents2, 
     ppMSents2, 
     ppRule
    ) where

import Control.Applicative((<$>))
import Control.Monad.Trans.State.Lazy
import Control.Monad


import qualified Data.Map as M

import Language.Carbon.Model
import Language.Carbon.HSentence

--
data PP = PP {buff :: String, 
              tabs :: Int}

----------------------------------------------------------------------------------------
--
ppExec :: State PP () -> String
ppExec p = 
    buff $ execState p PP{buff="",tabs=0}
--
putPpLn :: State PP () -> IO ()
putPpLn = putStrLn . ppExec 
--
pp :: String -> State PP ()
pp s = modify $ \p@PP{buff = b} ->
       p{buff = b ++ s}
--
ppNl  :: State PP ()
ppNl  = 
    modify $ \p@PP{buff = b, tabs = t} ->
       p{buff = b ++ "\r\n" ++ (concat $ take t $ repeat "  ")}
--
ppShow s = pp $ show s
--
ppAddTab :: State PP ()
ppAddTab = modify  $ \p@PP{tabs = t} ->
           p{tabs = t+1}
--
ppSubTab :: State PP ()
ppSubTab = modify $ \p@PP{tabs = t} ->
           p{tabs = t-1}
----------------------------------------------------------------------------------------
--
ppSents s = do
  pp "["
  sents s
  pp "]"
      where sents (h:t) | length t > 0 = ppSent h >> pp ", " >> sents t
                        | otherwise    = ppSent h
--
ppSents2 s = do
  pp "["
  sents s
  pp "]"
      where sents (h:t) | length t > 0 = ppNl >> ppSent h >> sents t
                        | otherwise    = ppSent h
--
ppMSents2 s = do
  pp "["
  sents s
  pp "]"
      where sents (h:t) | length t > 0 = ppNl >> ppMSent h >> sents t
                        | otherwise    = ppMSent h
--
ppRule r = do
    ppNl >> ppSent (ruleSignature r)
    pp ":"
    ppAddTab
    ppNl >> ppSents2 (ruleCondition r) 
    when (isAction r) $ do 
             pp " -> "
             ppAddTab
             ppNl >> ppMSents2 (actionEffect r)
    pp "."
--
ppMSent :: Sentence s => Modify s -> State PP () 
ppMSent ms = do
  pp $ if isAssert ms
       then "+"
       else "-"
  ppSent $ mdfArg ms
--
ppSent :: Sentence s => s -> State PP () 
ppSent s = do
  pp "( "
  mapM_ (\w -> ppWord w >> pp " ") $ toWords s
  pp ")"
--
ppWord :: Word -> State PP () 
ppWord w =
    case w of
      Key k -> pp k
      S   s -> ppShow s
      I   i -> ppShow i
      Var d n | d == STR   -> pp $ "?" ++ n
              | d == INT   -> pp $ "?!" ++ n
              | d == BOOL  -> pp $ "?&" ++ n
              | d == FLOAT -> pp $ "?@" ++ n
      Sub x y -> ppSent [x, Key "-", y]
      Sum xs -> ppSent (Key "+":xs)
------------------------------------------------------------------------
