module XGame.Model
    ( ) where

import Control.Applicative((<*), (<$>), (<*>))
import Language.Carbon.Model


-- Resource type model
data ResourceType = Energy | Ore deriving (Eq, Show)
-- Map ResourceType to Word
instance WordMorph ResourceType where
    toWord Energy = E 1 $ Domain "ResourceType"
    toWord Ore    = E 2 $ Domain "ResourceType"
-- Generic Schema of Data Representation of Example App
data GenSchema c o r a t = 
    Cell     c     |
    Core     o     |
    Resource r a t |
    At       c o   |
    Adjacent c c   |
    Move     o c   |
    Eat      o r a |
    Attack   o o a deriving (Eq, Show)
-- 
instance (WordMorph c, 
          WordMorph o, 
          WordMorph r, 
          WordMorph a, 
          WordMorph t) => Sentence (GenSchema c o r a t) where
    toWords (Cell id)         = [Key "Cell",     toWord id]
    toWords (Core id)         = [Key "Core",     toWord id]
    toWords (Resource id a t) = [Key "Resource", toWord id,   toWord a, toWord t]
    toWords (At cell core)    = [Key "At",       toWord cell, toWord core]
    toWords (Adjacent c1 c2)  = [Key "Adjacent", toWord c1, toWord c2]
    toWords (Move o c)        = [Key "Move",     toWord o, toWord c]
    toWords (Eat  o r a)      = [Key "Eat", toWord o, toWord r, toWord a]
    toWords (Attack o1 o2 a)  = [Key "Attack", toWord o1, toWord o2, toWord a]
--
data CellId = CidVar String | Cid (Int, Int) | CidNil deriving (Eq,Show)
data CoreId = OidVar String | Oid Int | OidNil deriving (Eq,Show)
data RscId  = RidVar String | Rid Int | RidNil deriving (Eq,Show)
instance WordMorph CellId where
    toWord (CidVar n)  = Var TUPLE n
    toWord (Cid (a,b)) = T [I a, I b]
    toWord CidNil      = Nil TUPLE
instance WordMorph CoreId where
    toWord (OidVar n) = Var INT n
    toWord (Oid a)    = I a
    toWord OidNil     = Nil INT
instance WordMorph RscId where
    toWord (RidVar n)  = Var INT n
    toWord (Rid a)     = I a
    toWord RidNil      = Nil INT

-- Concrete Schema for easy map on to Sentence
type Schema = GenSchema CellId CoreId RscId Int ResourceType

move_action :: CoreId -> CellId -> Rule Schema
move_action core to = 
    let from    = CidVar "from"
        preCond  = [At from core,
                    At to    OidNil,
                    Adjacent from to]
        postCond = [Retire $ At from core,
                    Retire $ At to   OidNil,
                    Assert $ At from OidNil,
                    Assert $ At to   core]
    in Action (Move core to) preCond (And []) postCond
------------------------------------------------------------------------------------------
test_data :: [Schema]
test_data =
    [Cell $ Cid (1,1), Cell $ Cid (1,2), Cell $ Cid (2,2)]
--
test_data2 :: [Schema]
test_data2 =
    let cells = Cell <$> Cid <$> [(1,1),(2,2),(3,3)]
        cores = Core <$> Oid <$> [1,2]
        ats   = [At (Cid (1,1)) OidNil]
        rscs  = [Resource (Rid 1) 100 Energy]
    in cells ++ cores ++ ats ++ rscs
--
test = toWords <$> test_data
--
test2 = toWords <$> test_data2
--
test3 = domainOf $ I 1
test4 = domainOf $ (Var BOOL "a" :: Word)
test5 = domainOf $ S "sdg"
