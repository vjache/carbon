{-# OPTIONS -XOverloadedStrings #-}
module XGame.MQ 
    (accept) where

import Network.AMQP

import qualified Data.ByteString.Lazy.Char8 as BL


accept = do
  conn <- openConnection "10.1.4.105" "/lps" "admin" "sifox2014"
  chan <- openChannel conn
    
  --declare queues, exchanges and bindings
  declareQueue chan newQueue {queueName = "t2"}
    
  declareExchange chan newExchange {exchangeName = "lpsc", exchangeType = "topic"}
  bindQueue chan "t2" "lpsc" "snmp"
    
  --subscribe to the queues
  consumeMsgs chan "t2" NoAck myCallbackDE
      
  getLine -- wait for keypress
  closeConnection conn
  putStrLn "connection closed"


myCallbackDE :: (Message,Envelope) -> IO ()
myCallbackDE (msg, env) = do
    putStrLn $ "received from DE: "++(BL.unpack $ msgBody msg)
--    ackEnv env
