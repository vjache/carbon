{-# OPTIONS_GHC -Wall #-}
module Main where

import System.Environment

import XGame.Model()
import XGame.MQ

-- | 'main' runs the main program
main :: IO ()
main = accept

--getArgs >>= print . haqify
 
haqify :: [String] -> String
haqify s = "Haq!111 " ++ (head s)
