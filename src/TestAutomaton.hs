-- file TestAutomaton.hs

{-# LANGUAGE 
  DeriveFunctor,
  FlexibleInstances #-}

import Test.Hspec
import Test.QuickCheck

import Control.Exception (evaluate)
import Control.Applicative((<$>))
import Control.Monad.State.Lazy
import qualified Data.Set as Set
import Data.List

import Language.Carbon.Model
import Language.Carbon.WorkingMemory.Automaton
import qualified Language.Carbon.WorkingMemory.SpNet as SpNet
import Language.Carbon.WorkingMemory.SolutionPlan


main :: IO ()
main = hspec $ do
         let getActions = (mdfArg <$>) . fst                                  
         describe "SRM with 'move' rule against:" $ do
                 let lastActivatedActions dataset = 
                         mdfArg <$> (fst $ (srm moveRule) 
                                     `testAgainst` dataset)
                 it "1-Moving-Core data set" $ do
                   let acts = lastActivatedActions assertMoveCondMin
                   acts `shouldSatisfy` (1 ==) . length
                   (head <$> ruleSignature <$> acts) `shouldSatisfy` all (== Key "move")
                 it "2-Moving-Core data set" $ do
                   let acts = lastActivatedActions assertMove2CoresCond
                   acts `shouldSatisfy` (2 ==) . length
                   (head <$> ruleSignature <$> acts) `shouldSatisfy` all (== Key "move")
         describe "SRM with 'prod' rule against:" $ do
                 let lastActivatedActions dataset = 
                         mdfArg <$> (fst $ (srm prodRule) 
                                     `testAgainst` dataset)
                 it "1-Cell data set" $ do
                   let acts = lastActivatedActions assertProdMin
                   acts `shouldSatisfy` (1 ==) . length
                 it "2-Cell data set" $ do
                   let acts = lastActivatedActions assertProd2Cell
                   acts `shouldSatisfy` (3 ==) . length
         describe "SRM with symmetric 'near' iff-rule against:" $ do
                 it "1-Near sentece data set" $ do
                   let (res, au) = (srm nearSymmetryRule) `testAgainst` assertNearMin
                   res `shouldSatisfy` ([] ==)
                   au `shouldSatisfy` member [Key "Near", wstr "0:0", wstr "1:1"]
                   au `shouldSatisfy` member [Key "Near", wstr "1:1", wstr "0:0"]
         describe "SRM with transitive 'reachable' if-rule against:" $ do
                 it "simple reacability graph data set" $ do
                   let (res, au) = (srm reachableTransRule) 
                                    `testAgainst` assertReachableMin
                   res `shouldSatisfy` ([] ==)
                   au `shouldSatisfy` member [Key "Reachable", wstr "1", wstr "3"]
                   au `shouldSatisfy` member [Key "Reachable", wstr "1", wstr "4"]
                   au `shouldSatisfy` member [Key "Reachable", wstr "2", wstr "4"]
                   (matchPattern 
                    [Key "Reachable", var "x", var "y"] $ 
                    enlist au) `shouldSatisfy` (7 ==) . length
-------------------------------------------------------------------------------
         describe "MRM with transitive & symmetric 'reachable' if-rule against:" $ do
                 it "simple reacability graph data set" $ do
                   let (res, au) = (mrm [reachableTransRule,
                                          reachableSymmRule]) `testAgainst` 
                                    assertReachableMin
                   res `shouldSatisfy` ([] ==)
                   au `shouldSatisfy` member [Key "Reachable", wstr "1", wstr "3"]
                   au `shouldSatisfy` member [Key "Reachable", wstr "3", wstr "1"]
                   au `shouldSatisfy` member [Key "Reachable", wstr "1", wstr "4"]
                   au `shouldSatisfy` member [Key "Reachable", wstr "4", wstr "1"]
                   au `shouldSatisfy` member [Key "Reachable", wstr "2", wstr "4"]
                   au `shouldSatisfy` member [Key "Reachable", wstr "4", wstr "2"]
                   au `shouldSatisfy` not .  member [Key "Reachable", wstr "1", wstr "1"]
                   (matchPattern 
                    [Key "Reachable", var "x", var "y"] $ 
                    enlist au) `shouldSatisfy` (14 ==) . length
                   (matchPattern 
                    [Key "Reachable", var "x", var "x"] $ 
                    enlist au) `shouldSatisfy` (0 ==) . length
         describe "MRM with 'move' & 'near-symmetry' rules against incomplete dataset:" 
             $ do
                 it "'move-core' rule is not activated if no 'near-symmetry' rule" $ do
                   getActions (mrm [moveRule] `testAgainst` 
                               assertMoveCondMin2) `shouldSatisfy` (0 ==) . length
                 it "'move-core' rule is activated due to 'near-symmetry' rule" $ do
                   getActions (mrm [moveRule,nearSymmetryRule] `testAgainst` 
                               assertMoveCondMin2) `shouldSatisfy` (1 ==) . length
         describe "SRM with arith containing 'eat' action-rule against:" $ do
                 it "minimal activating data set" $ do
                   let (res, au) = (srm eatRule) `testAgainst` assertEatMin
                       effectAsserts = mdfArg <$> 
                                       (asserts $ actionEffect $ mdfArg $ head res)
                       effectRetires = mdfArg <$> 
                                       (retires $ actionEffect $ mdfArg $ head res)
                   res `shouldSatisfy` (1 ==) . length
                   select [ivar "rscAmount"] 
                              (matchPattern 
                               [Key "Rsc", wstr "RS1",  ivar "rscAmount"] $ 
                               effectRetires) `shouldSatisfy` (I 5 ==) . last . last
                   select [ivar "rscAmount"] 
                              (matchPattern 
                               [Key "Rsc", wstr "RS1",  ivar "rscAmount"] $ 
                               effectAsserts) `shouldSatisfy` (I 4 ==) . last . last
         describe "SRM with aggregated 'explode' action-rule against:" $ do
                 it "minimal activating data set" $ do
                   let (trace, au) = (srm explodeRule) 
                                      `interpretAgainst` assertExplodeMin
                       effect = mdfArg <$> (actionEffect $ head $ agenda au)
                   (matchPattern [Key "Foe", var "foe"] effect)
                      `shouldSatisfy` (2 ==) . length
                   (matchPattern [Key "At",    var "foe",  var "cell"] effect)
                      `shouldSatisfy` (2 ==) . length
-------------------------------------------------------------------------------
-- TEST helper functions
-------------------------------------------------------------------------------
select :: [Word] -> [VarBindings] -> [[Word]]
select ws vbs = do
    vb <- vbs
    return $ getValue vb <$> ws
wint :: Int -> Word
wint = toWord

wstr :: String -> Word
wstr = toWord

var :: String -> Word
var = Var STR

ivar :: String -> Word
ivar = Var INT
--
evalAutomaton = (flip evalState) newAutomaton
runAutomaton  = (flip runState) newAutomaton
--
takeLastSolutions :: ([SpNet.Result], Automaton [Word]) -> [VarBindings]
takeLastSolutions = snd . mdfArg . last . fst
--
lastApplyResults = last . fst
--
testAgainst :: Module [Word] -> [ Modify [Word] ] -> 
               ([Modify (Rule [Word])], Automaton [Word])
testAgainst mdl sents =
    let (trace, automaton) = interpretAgainst mdl sents
    in (last trace, automaton)
--
applyAgainst
  :: (Ord s, Sentence s, Substitutable s) =>
     Module s -> [Modify s] -> ([[SpNet.Result]], Automaton s)
applyAgainst mdl sents = runAutomaton $ do
  compileModule mdl
  mapM apply sents
--
interpretAgainst  :: (Ord s, Sentence s, Substitutable s, ArithEvaluable s) =>
     Module s -> [Modify s] -> ([[Modify (Rule s)]], Automaton s)
interpretAgainst mdl sents = runAutomaton $ do
  compileModule mdl
--  mapM (interpret <=< apply) sents
  mapM handle sents
-- SRM is (S)ingle (R)ule (M)odule
srm :: Rule [Word] -> Module [Word]
srm r =  newModule {modRules = [r]}
-- MRM is (M)ulty  (R)ule (M)odule
mrm :: [Rule [Word]] -> Module [Word]
mrm rs =  newModule {modRules = rs}
-------------------------------------------------------------------------------
-- TEST DATA: Test data sets to test rules against
-------------------------------------------------------------------------------
-- Assert minimal set of sentences to satisfy 'move' rule condition
assertMoveCondMin = 
    Assert <$> [[Key "Core", wstr "R1"  ],
                [Key "Cell", wstr "0:0" ],
                [Key "Cell", wstr "1:1" ],
                [Key "At"  , wstr "R1",  wstr "0:0"],
                [Key "Near", wstr "0:0", wstr "1:1"],
                [Key "Empty", wstr "1:1"] ]
-- Reverse 'Near' sentence
assertMoveCondMin2 = 
    Assert <$> [[Key "Core", wstr "R1"  ],
                [Key "Cell", wstr "0:0" ],
                [Key "Cell", wstr "1:1" ],
                [Key "At"  , wstr "R1",  wstr "0:0"],
                [Key "Near", wstr "1:1", wstr "0:0"],
                [Key "Empty", wstr "1:1"] ]
assertMove2CoresCond = 
    Assert <$> [[Key "Core", wstr "R1"  ],
                [Key "Core", wstr "R2"  ],
                [Key "Cell", wstr "0:0" ],
                [Key "Cell", wstr "1:1" ],
                [Key "Cell", wstr "2:2" ],
                [Key "At"  , wstr "R1",  wstr "0:0"],
                [Key "At"  , wstr "R2",  wstr "2:2"],
                [Key "Near", wstr "0:0", wstr "1:1"],
                [Key "Near", wstr "2:2", wstr "1:1"],
                [Key "Empty", wstr "1:1"] ]
assertProdMin =
    Assert <$> [ [Key "Cell", wstr "0:0" ] ]
assertProd2Cell =
    Assert <$> [ [Key "Cell", wstr "0:0" ],
                 [Key "Cell", wstr "1:1" ] ]
assertNearMin = 
    Assert <$> [ [Key "Near", wstr "0:0", wstr "1:1"] ]
assertReachableMin = 
    Assert <$> [ [Key "Reachable", wstr "1", wstr "2"],
                 [Key "Reachable", wstr "2", wstr "3"],
                 [Key "Reachable", wstr "3", wstr "4"],
                 [Key "Reachable", wstr "5", wstr "6"] ]
assertEatMin =
    Assert <$> [ [Key "Core", wstr "C1",  I 10       ],
                 [Key "At",   wstr "C1",  wstr "0:0" ],
                 [Key "At",   wstr "RS1", wstr "0:0" ],
                 [Key "Rsc",  wstr "RS1", I 5        ] ]
assertExplodeMin =
    Assert <$> [ 
 [Key "Bomb",  wstr "B1"],
 [Key "At",    wstr "B1", wstr "0:0"],
 [Key "Near",  wstr "0:0", wstr "0:1"],
 [Key "Near",  wstr "0:0", wstr "1:0"],
 [Key "Near",  wstr "0:0", wstr "1:1"],
 [Key "At",    wstr "F1",  wstr "0:1"],
 [Key "At",    wstr "F2",  wstr "1:0"],
 [Key "Foe",   wstr "F1"],
 [Key "Foe",   wstr "F2"] ]
-------------------------------------------------------------------------------
-- TEST RULES: Test rules used to construct SRMs & MRMs for tests
-------------------------------------------------------------------------------
--
moveRule :: Rule [Word]
moveRule = 
    let signature = [Key "move", var "coreId", var "dstCellId"]
        condition = [ [Key "Core", var "coreId"    ],
                      [Key "Cell", var "dstCellId" ],
                      [Key "At",   var "coreId",    var "srcCellId"],
                      [Key "Near", var "srcCellId", var "dstCellId"],
                      [Key "Empty",var "dstCellId"] ]
        guard = And []
        effect    = []
    in Action signature condition guard effect
--
{-
  eat(CoreId, RscId):
     (core CoreId RscLevel)
     (at CoreId CellId)
     (at RscId  CellId)
     (rsc RscId Amount) where Amount > 0 ->
        (at   CoreId CellId)
        (rsc  RscId  (AmountId - 1))
        (core CoreId (RscLevel + 1))
 -}
{-
  a || b 
  b || c
  (if (x || y) ^ (y || z) then (x || z)) => a || c
-------------
  a || b
  a || c
  b not|| c <=> c not|| b
  (if (x || y) ^ (y not || z) then (x not|| z))
---------------------

-- Right usage of aggregated action:
-- Match all axis parallel to X
-- Match all axis or to X
-- Make former parallel axis to X become perpendicular 
-- Make former perpendicular axis to X become parallel

rotateCW90(X):
   ('||' X Y)
   ('_|_' X Z) -> 
      +('_|_' X Y) 
      +('||' X Z).

-- Potentially wrong usage of aggregated action:
-- 1. Match some block
-- 2. Put it to position X
-- But actually it will be a:
-- 1. Match all free blocks
-- 2. Put all blocks to the position X

put_block_to(X):
   (block_free B) ->
      (block_at B X).

-- Right usage of aggregated action:
-- 1. Match bomb position X
-- 2. Match locations which is near X
-- 3. Match foes on near locations
-- 4. Remove those foes and bomb
explode(B):
    (bomb_at B X)
    (near X Y)
    (foe_at F Y) -> 
       -(bomb_at B X)
       -(foe_at F Y).
near(X,Y):
   (adjacent X Y).
near(X,X):
   (cell X).
 -}

explodeRule :: Rule [Word]
explodeRule =
    let signature = [Key "explode", var "bombId"]
        condition = [ [Key "Bomb",  var "bombId"],
                      [Key "At",    var "bombId", var "cellId"],
                      [Key "Near",  var "cellId", var "cellId1"],
                      [Key "At",    var "foeId",  var "cellId1"],
                      [Key "Foe",   var "foeId"] ]
        guard     = And []
        effect    = Retire <$> [ [Key "Bomb",  var "bombId"],
                                 [Key "At",    var "foeId",  var "cellId1"],
                                 [Key "Foe",   var "foeId"] ]
    in Action signature condition guard effect

eatRule :: Rule [Word]
eatRule = 
    let signature = [Key "eat", var "coreId", var "rscId"]
        condition = [ [Key "Core", var "coreId", ivar "rscLevel" ],
                      [Key "At",   var "coreId", var "cellId"   ],
                      [Key "At",   var "rscId",  var "cellId"   ],
                      [Key "Rsc",  var "rscId",  ivar "rscAmount"] ]
        guard     = Pred [Key "gr", ivar "rscAmount", I 0]
        effect    = [ Retire [Key "Rsc",  var "rscId",  ivar "rscAmount"],
                      Assert [Key "Rsc",  var "rscId", 
                              Sub (ivar "rscAmount") (I 1)],
                      Assert [Key "Core",  var "coreId",  
                              Sum [ivar "rscLevel", I 1]]] -- TODO: test arith works
    in Action signature condition guard effect
--
prodRule :: Rule [Word]
prodRule = 
    let signature = [Key "prod", var "cellId1", var "cellId2"]
        condition = [ [Key "Cell", var "cellId1" ],
                      [Key "Cell", var "cellId2" ] ]
        guard = And []
        effect    = []
    in Action signature condition guard effect
--
nearSymmetryRule :: Rule [Word]
nearSymmetryRule = 
    let signature =   [Key "Near", var "dstCellId", var "srcCellId"]
        condition = [ [Key "Near", var "srcCellId", var "dstCellId"] ]
        guard = And []
    in Iff signature condition guard
reachableTransRule :: Rule [Word]
reachableTransRule = 
    let signature =   [Key "Reachable", var "x", var "y"]
        condition = [ [Key "Reachable", var "x", var "z"],
                      [Key "Reachable", var "z", var "y"] ]
        guard = Pred [Key "distinct", var "x", var "y", var "z"]
    in If signature condition guard
reachableSymmRule :: Rule [Word]
reachableSymmRule = 
    let signature =   [Key "Reachable", var "x", var "y"]
        condition = [ [Key "Reachable", var "y", var "x"] ]
        guard     = Pred [Key "neq", var "x", var "y"]
    in If signature condition guard
---------------------------------------------------------------------------------
-- DBG functions
---------------------------------------------------------------------------------
--
{--
dbgOSR = let testRes = (srm moveRule) `testAgainst` assertMoveCondMin
             h       = fst $ mdfArg $ last $ fst testRes
             spn     = srSpNet $ snd testRes
         in spPP "" $ SpNet.toSpTree spn h
--}
--
dbgMove2Cores = (srm moveRule) `interpretAgainst` assertMove2CoresCond
--
ppTrace :: [[SpNet.Result]] -> IO ()
ppTrace tr = do
    let nums = iterate (+1) 1 :: [Int]
        numerated = zip tr nums
        tab1 = "  "
        tab2 = "    "
        tab3 = "      "
    forM_ numerated $ 
             \(rs,n) -> do
               putStrLn $ (show n) ++ "."
               forM_ rs $ \ r -> do
                          let dec = if isAssert r then "+" else "-"
                              h   = fst $ mdfArg r 
                              vbs = snd $ mdfArg r 
                          putStrLn $ tab1 ++ dec ++ " " ++ (show h)
                          forM_ vbs $ \vb -> do
                                      putStr $ tab2 
                                      putStrLn $ ppShowVb vb
--
ppRuleTrace :: [[Modify (Rule [Word])]] -> IO ()
ppRuleTrace  tr = do
    let nums = iterate (+1) 1 :: [Int]
        numerated = zip tr nums
        tab1 = "  "
        tab2 = "    "
        tab3 = "      "
    forM_ numerated $ 
             \(rs,n) -> do
               putStrLn $ (show n) ++ "."
               forM_ rs $ \ mr -> do
                          putStrLn $ ppShowRule tab1 mr
--
ppShowRule shift mrule = concat $
    let dec  = if isAssert mrule then "+" else "-"
        rule = mdfArg mrule
        shift1 = shift ++ "  "
    in
      [shift, dec, ppShowSentence $ ruleSignature rule, ":\r\n",
       shift, "  "] 
      ++ 
      (intersperse ("\r\n" ++ shift1) $ ppShowSentence <$> ruleCondition rule)
      ++ case rule of
           Action{} -> ["\r\n"  ++ shift1 ++ "->"]
           Goal{} -> [""]
    
--
ppShowSentence s = 
    "( " ++ (concat $ intersperse " " $ ppShowWord <$> toWords s) ++ " )"
--
ppShowVb :: VarBindings -> String
ppShowVb vb = concat $ do
  (var,val) <- vb
  [(ppShowWord var)," = ",(ppShowWord val),", "]
--
ppShowWord (Var _ name) = "?" ++ name
ppShowWord (S val) = "\"" ++ val ++ "\""
ppShowWord (I val) = show val
ppShowWord (Key key) = key
