module Data.List.Util 
    ( separate ) where

import Data.List

--
separate :: [(a -> Bool)] -> [a] -> [[a]]
separate [] as      = [as]
separate (f:fs) as = 
    let (sat, unsat) = partition f as
    in sat:separate fs unsat
