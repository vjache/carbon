module Data.MapOnSets
    (
     MapOnSets,
     lookup,
     enlistValuesFor,
     insert,
     delete,
     fromList,
     hasKey
    ) where

import Prelude hiding (lookup)
import qualified Data.Set as S
import qualified Data.Map as M

----------------------------------------------------------------------------------
type MapOnSets k v = M.Map k (S.Set v)

lookup :: (Ord k, Ord v) => k -> MapOnSets k v -> S.Set v
lookup k mp = maybe S.empty id $ M.lookup k mp

hasKey :: (Ord k, Ord v) => k -> MapOnSets k v -> Bool
hasKey = M.member

enlistValuesFor :: (Ord k, Ord v) => k -> MapOnSets k v -> [v]
enlistValuesFor k mp = maybe [] S.toList $ M.lookup k mp


insert :: (Ord k, Ord v) => k -> v -> MapOnSets k v -> MapOnSets k v
insert k v mp =
    let f Nothing  = Just $ S.singleton v
        f (Just s) = Just $ S.insert v s
    in M.alter f k mp

delete :: (Ord k, Ord v) => k -> v -> MapOnSets k v -> MapOnSets k v
delete k v mp =
    let f Nothing  = Nothing
        f (Just s) = let s' = S.delete v s
                     in if   S.null s' 
                        then Nothing 
                        else Just s'
    in M.alter f k mp

fromList :: (Ord k, Ord v) => [(k,v)] -> MapOnSets k v
fromList kvs = 
    foldr (\(k,v)-> insert k v) M.empty kvs
