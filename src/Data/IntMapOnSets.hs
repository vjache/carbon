module Data.IntMapOnSets
    (
     MapOnSets,
     lookup,
     enlistValuesFor,
     insert,
     delete,
     fromList,
     hasKey,
     empty
    ) where

import Prelude hiding (lookup)
import qualified Data.Set as S
import qualified Data.IntMap as M

----------------------------------------------------------------------------------
type MapOnSets v = M.IntMap (S.Set v)

lookup :: Ord v => Int -> MapOnSets v -> S.Set v
lookup k mp = maybe S.empty id $ M.lookup k mp

hasKey :: Ord v => Int -> MapOnSets v -> Bool
hasKey = M.member

enlistValuesFor :: Ord v => Int -> MapOnSets v -> [v]
enlistValuesFor k mp = maybe [] S.toList $ M.lookup k mp


insert :: Ord v => Int -> v -> MapOnSets v -> MapOnSets v
insert k v mp =
    let f Nothing  = Just $ S.singleton v
        f (Just s) = Just $ S.insert v s
    in M.alter f k mp

delete :: Ord v => Int -> v -> MapOnSets v -> MapOnSets v
delete k v mp =
    let f Nothing  = Nothing
        f (Just s) = let s' = S.delete v s
                     in if   S.null s' 
                        then Nothing 
                        else Just s'
    in M.alter f k mp

fromList :: Ord v => [(Int,v)] -> MapOnSets v
fromList kvs = 
    foldr (\(k,v)-> insert k v) M.empty kvs

empty :: Ord v => MapOnSets v
empty = M.empty
