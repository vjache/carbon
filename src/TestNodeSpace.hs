-- file TestNodeSpace.hs

{-# LANGUAGE 
  DeriveFunctor,
  FlexibleInstances #-}

import Test.Hspec
import Test.QuickCheck

import Control.Exception (evaluate)
import Control.Applicative((<$>))
import Control.Monad.Trans.State.Lazy
import Control.Monad
import qualified Data.Set as Set
import Data.List
import Data.Maybe
import qualified Data.Map.Lazy as M
import qualified Data.IntSet as IS

import Language.Carbon.Model
import Language.Carbon.DSL
import Language.Carbon.HSentence
import Language.Carbon.WorkingMemory.Search
import Language.Carbon.WorkingMemory.Search.Pure
import qualified Language.Carbon.WorkingMemory.Search.Concurrent as SC
import Language.Carbon.WorkingMemory.Automaton
import qualified Language.Carbon.WorkingMemory.SpNet as SpNet
import Language.Carbon.WorkingMemory.SolutionPlan
import qualified Language.Carbon.WorkingMemory.NodeSpace as N

import Language.Carbon.PP


import Control.Parallel (par, pseq)
import Control.Parallel.Strategies
import Control.DeepSeq
import Control.Concurrent.STM
import Control.Concurrent
import Control.Exception (bracket_)

import Debug.Trace
import GHC.Conc (labelThread)

------------------------------------------------------------------------------------
eventIO :: String -> IO a -> IO a
eventIO label =
  bracket_ (traceEventIO $ "START " ++ label)
           (traceEventIO $ "STOP "  ++ label)

main :: IO ()
main = do 
  tid <- myThreadId
  labelThread tid $ "MAIN-THREAD"
  hspec $ do
         describe "Lab Rat Escape." $ do
            let init p = let mdl = fromSentence <$> ratGameModule
                             n0 = N.rootNodeFromModule mdl
                         in N.update (Assert <$> fromSentence <$> p) n0
            it "Problem One. Priority search." $ 
               do labelMyThread "MAIN-TEST-THREAD-1" 
                  let n       = init problemOne
                  let (ns,ss) = runSearch $ searchPrio distPrio [n]
                  let goals   = fetchGoals $ ns
                  let r@(a,g) = fetchExitGoal goals
                  eventIO "Print-Result-1" $ printResult r
            it "Problem Two. Priority search." $ 
               do labelMyThread "MAIN-TEST-THREAD-2" 
                  let n       = init problemTwo
                  let (ns,ss) = runSearch $ searchPrio distPrio [n]
                  let goals   = fetchGoals $ ns
                  let r@(a,g) = fetchExitGoal goals
                  eventIO "Print-Result-2" $ printResult r
            it "Problem Two. Priority search. Search concurrently." $ 
               do labelMyThread "MAIN-TEST-THREAD-3"
                  let n      = init problemTwo
                  eventIO "EVAL-wmHash" $ evaluate $ force $ N.wmHash n
                  (rq, kill) <- eventIO "runSearch" $ SC.runSearch distPrio n
                  eventIO "Fetch-Exit-Goal" $ 
                          do r <- fetchExitGoalQ rq
                             kill
                             printResult r
--
-- Prioritizes s-nodes based on a distance from root s-node. 
-- Also it handles a special cases:
--  * if head of address encontered in a tail of 
--    address then such a node gains lowest priority
--  * if head of address is a ("eat" _ _) action then it gains pretty high priority
--  * if none of the cases above is takes place then node gains a normal priority
--
distPrio :: SearchSession ss => N.Node -> ss Prio
distPrio n =
    return $ case N.address n of
               addr@(k:t) | elem k t  -> PrioMin
                          | otherwise ->
                              case toWords k of 
                                Key "eat":_ -> Prio High   (-(length addr))
                                _           -> Prio Normal (-(length addr))
               [] -> 
                   PrioMax
--
labelMyThread lbl = do
  tid <- myThreadId
  labelThread tid lbl
--
traceShowId x = traceShow x x
--
printNodes ns = 
    forM_ ns $ putPpLn . (ppNode 1)    
--
printResult (addr, grule) =
    putPpLn $ do 
      ppNl >> (ppRule grule)
      ppNl >> ppSents2 addr
      ppNl >> ppShow (length addr)
--
sigStartsWith k sig = case toWords sig of
              Key k1:_ -> k == k1
              _ -> False
--
fetchExitGoal agoals = 
    fromJust $ find ((sigStartsWith "ExitFound") . 
                     ruleSignature . snd) $ agoals -- runEval $ parList rdeepseq agoals
--
fetchExitGoalQ agoalsQ = do
  (addr, goals) <- atomically $ readTBQueue agoalsQ
  let mrs = find ((sigStartsWith "ExitFound") . 
                  ruleSignature) $ goals -- runEval $ parList rdeepseq agoals
  --traceShow ("RESULT: ", (length addr, mhead addr), length goals) $ 
  case mrs of
    Just g  -> return (addr, g)
    Nothing -> fetchExitGoalQ agoalsQ
    where mhead (h:_) = Just h
          mhead []    = Nothing
--
fetchExitGoalQ2 agoalsQ = do
  (addr, goals) <- atomically $ readTBQueue agoalsQ
  if length addr == 22
  then return goals
  else fetchExitGoalQ2 agoalsQ
--
ratGameModule :: Module [Word]
ratGameModule = 
    mrm $ evalRuleBuilder <$> 
            [moveAction, 
             eatAction, 
             nearSymmetryRule, 
             exitGoal, deathGoal]
    where
      --
      moveAction = do
        action (k"move" ?"ratId" ?"cellId")
        when_ #(k"Rat" ?"ratId" ?!"ratE")
              &(k"Cell" ?"cellId")
              &(k"Cell" ?"cellId0")
              &(k"Near" ?"cellId" ?"cellId0")
              #(k"At" ?"ratId" ?"cellId0")
              #(k"Free" ?"cellId")
        guard_ $ Pred (k"gr" ?!"ratE" !I 0)
        effect &(k"Rat"  ?"ratId" !(k"-" ?!"ratE" !I 1))
               &(k"Free" ?"cellId0")
               &(k"At"   ?"ratId" ?"cellId")
      --
      eatAction = do 
        action (k"eat" ?"ratId")
        when_ #(k"Rat"  ?"ratId" ?!"ratE")
              &(k"At"   ?"ratId"  ?"cellId0")
              &(k"Cell" ?"cellId0")
              #(k"At"   ?"foodId" ?"cellId0")
              #(k"Food" ?"foodId" ?!"qty")
        guard_ $ Pred (k"gr" ?!"qty" !I 0)
        effect &(k"Rat" ?"ratId" !(k"+" ?!"ratE" ?!"qty"))
      --
      nearSymmetryRule = do
        if_   (k"Near" ?"cid0" ?"cid1")
        when_ &(k"Near" ?"cid1" ?"cid0")
      --
      exitGoal = do 
        goal (k"ExitFound" ?"ratId"       )
        when_ &(k"Rat"  ?"ratId" ?!"ratE"   )
              &(k"Cell" ?"cellId"          )
              &(k"Exit" ?"cellId"          )
              &(k"At"   ?"ratId" ?"cellId" )
        guard_ $ Pred (k"gr" ?!"ratE" !I 0)
      --
      deathGoal = do 
        goal  (k"Death" ?"ratId")
        when_ &(k"Rat"   ?"ratId" !I 0)
-------------------------------------------------------------------------------
-- TEST helper functions
-------------------------------------------------------------------------------
iword :: Int -> Word
iword = toWord

sword :: String -> Word
sword = toWord

svar :: String -> Word
svar = Var STR

ivar :: String -> Word
ivar = Var INT
-- MRM is (M)ulty  (R)ule (M)odule
mrm :: [Rule [Word]] -> Module [Word]
mrm rs =  newModule {modRules = rs}
-------------------------------------------------------------------------------
-- TEST DATA: Test data sets to test rules against
-------------------------------------------------------------------------------
cellId x y = sword $ show x ++ ":" ++ show y
--
keyCell = Key "Cell"
keyFree = Key "Free"
keyNear = Key "Near"
--
freeCell x y = 
    let cid = cellId x y
    in [ [keyCell, cid], [keyFree, cid] ]
--
nearCells x y x1 y1 =
    [[keyNear, cellId x y, cellId x1 y1]]

-- | 10 x 10 cells desk
desk = let xsize = 100
           ysize = 20
       in (freeCell xsize ysize) ++
          concat [freeCell x ysize ++
                  nearCells x ysize (x+1) ysize | x <- [1..(xsize-1)]] ++
          concat [freeCell xsize y ++ 
                  nearCells xsize y xsize (y+1) | y <- [1..(ysize-1)]] ++
          do 
            x <- [1..(xsize-1)]
            y <- [1..(ysize-1)]
            ((freeCell x y) ++
             (nearCells x y (x+1) y) ++
             (nearCells x y x (y+1)))
--
obstaclesX3Y1to7_9to10 =
    let freeCells = do y <- [1..7]++[9..10]
                       [[keyFree, cellId 3 y]]
    in (\\ freeCells)
--
ratAtX1Y1 e d = 
    let c0  = cellId 1 1
        add = [[Key "Rat",  sword "r1", I e ],
               [Key "At",   sword "r1", c0]]
        del = [[keyFree, c0]]
    in (d \\ del) ++ add
--
foodAtX4Y7 e d = d ++ [[Key "Food", sword "f1", iword e],
                     [Key "At",   sword "f1", cellId 4 7]]
--
setExitX10Y10 d =
    d ++ [[Key "Exit", cellId 10 10]]
--
-- |Rat with energy 20 at cell (1,1) finds path to exit at (10,10)
-- 
-- | R . O . . . . . . . |
-- | . . O . . . . . . . |
-- | . . O . . . . . . . |
-- | . . O . . . . . . . |
-- | . . O . . . . . . . |
-- | . . O . . . . . . . |
-- | . . O . . . . . . . |
-- | . . . . . . . . . . |
-- | . . O . . . . . . . |
-- | . . O . . . . . . X |
problemOne = setExitX10Y10 $ ratAtX1Y1 20 $ obstaclesX3Y1to7_9to10 desk
-- |Rat with energy 15 at cell (1,1) finds path to exit at (10,10) 
-- |passing through food at (4,7)
-- 
-- | R . O . . . . . . . |
-- | . . O . . . . . . . |
-- | . . O . . . . . . . |
-- | . . O . . . . . . . |
-- | . . O . . . . . . . |
-- | . . O . . . . . . . |
-- | . . O F . . . . . . |
-- | . . . . . . . . . . |
-- | . . O . . . . . . . |
-- | . . O . . . . . . X |
problemTwo = foodAtX4Y7 10 $ setExitX10Y10 $ ratAtX1Y1 15 $ obstaclesX3Y1to7_9to10 desk
-----------------------------------------------------------------------------------------
ppNode :: Int -> N.Node -> State PP ()
ppNode d n = do
  ppNl >> pp "node: " 
  ppShow (length $ N.address n) >> (ppSents $ N.address n)
  when (d > 1) $ do
    ppNl >> pp "hash: " >> (ppShow $ N.wmHash n)
    ppNl >> pp "children:"
    ppAddTab
    forM_ (M.elems $ N.children n) $ 
              \n' -> ppNode (d-1) n'
    ppSubTab
